(function() {
    document.body.ondrop = function(event) {
        event.preventDefault();
        event.stopPropagation()
    };
    var diyData = {};

    function diyPhone(temp, data) {
        diyData = temp;
        this.init(data)
    }
	
    diyPhone.prototype = {
        init: function(data) {
            new Vue({
                el: '#app',
                data: {
                    diyData: data,
					scene:'',	//场景说明
                    selectedIndex: -1
                },
                methods: {
					//添加组件
                    onAddItem: function(key) {
						if(this.diyData.values.length==5){
							$.show_error('最多添加5项');
							return false;
						}
						//去除重复
						for(var idx in this.diyData.values){
							if(this.diyData.values[idx].kid == diyData[key].kid){
								$.show_error('该项添加过了');
								return false;
							}
						}
                        var data = $.extend(true, {}, diyData[key]);
                        this.diyData.values.push(data);
                    },
					//拖拽组件
                    onDragItemEnd: function(DragItem) {
                        DragItem.newIndex
                    },
					//删除组件
                    onDeleleItem: function(index) {
                        var that = this;
						if(that.diyData.values.length==1){
							$.show_error('至少保留1项');
							return false;
						}
						layer.confirm('你确定要删除吗？',{
                            icon: 0,
                            btn:['取消','确定'],
                            title: '友情提示',
                            skin: 'layui-layer-hema',
                            cancel : function(){
                                // 你点击右上角 X 回调
                            },
                            btn1:function(temp,layero){
								layer.close(temp);
                            },
                            btn2:function(temp){
                                that.diyData.values.splice(index, 1);
								that.selectedIndex = -1;
                                layer.close(temp);
                            },
                            end:function() {
                                //所有操作都会执行
							}
						})
                    },
					//提交保存
                    onSubmit: function(){
						if(this.diyData.values.length==0){
							$.show_error('至少要添加1项');
							return false;
						}
                        if (this.diyData.sceneDesc == '') {
                            $.show_error('场景使用说明不可为空');
							return false;
                        };
                        $.post('', {
							data: this.diyData
                        }, function(result) {
                            if(typeof(result)=='string'){
                                result = JSON.parse(result);
                            }
                            result.code == 1 ? $.show_success(result.msg, result.url) : $.show_error(result.msg)
                        })
                    }
                }
            })
        }
    };
    window.diyPhone = diyPhone
})(window)