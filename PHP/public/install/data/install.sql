-- ----------------------------
-- 用户
-- ----------------------------
DROP TABLE IF EXISTS `hema_user`;
CREATE TABLE `hema_user` (
  `user_id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT '主键id',
  `union_id` varchar(50) NOT NULL DEFAULT '' COMMENT '平台唯一标识',
  `open_id` varchar(50) NOT NULL DEFAULT '' COMMENT 'openid(唯一标识)',
  `user_name` varchar(255) NOT NULL DEFAULT '' COMMENT '用户名',
  `password` varchar(255) NOT NULL DEFAULT '' COMMENT '登录密码',
  `nickname` varchar(255) NOT NULL DEFAULT '' COMMENT '昵称',
  `avatar` varchar(255) NOT NULL DEFAULT '' COMMENT '头像',
  `gender` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT '性别',
  `country` varchar(50) NOT NULL DEFAULT '' COMMENT '国家',
  `province` varchar(50) NOT NULL DEFAULT '' COMMENT '省份',
  `city` varchar(50) NOT NULL DEFAULT '' COMMENT '城市',
  `money` decimal(10,2) unsigned NOT NULL DEFAULT '0.00' COMMENT '钱包余额',
  `linkman` varchar(20) NOT NULL DEFAULT '' COMMENT '联系人',
  `phone` varchar(20) NOT NULL DEFAULT '' COMMENT '联系电话',
  `pay` decimal(10,2) unsigned NOT NULL DEFAULT '0.00' COMMENT '消费金额',
  `score` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '用户积分',
  `converted` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '已兑换积分',
  `v` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT '会员等级',
  `birthday` varchar(50) NOT NULL DEFAULT '' COMMENT '会员生日',
  `address_id` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '默认收货地址',
  `recommender` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '推荐人',
  `status` tinyint(3) unsigned NOT NULL DEFAULT '20' COMMENT '身份 10用户 20管理 30代理',
  `is_staff` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT '内部员工 0=否 1是',
  `platform` tinyint(3) unsigned NOT NULL DEFAULT '10' COMMENT '平台 10微信 20H5',
  `is_subscribe` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT '是否关注公众号',
  `agent_id` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '所属代理',
  `login_count` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '登录次数',
  `applet_id` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '小程序id',
  `create_time` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '创建时间',
  `update_time` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '更新时间',
  PRIMARY KEY (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=10002 DEFAULT CHARSET=utf8 COMMENT='用户表';
-- ----------------------------
-- 交易记录
-- ----------------------------
DROP TABLE IF EXISTS `hema_record`;
CREATE TABLE `hema_record` (
  `record_id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT '主键ID',
  `user_type` tinyint(3) unsigned NOT NULL DEFAULT '10' COMMENT '用户类型(10商家会员 20站点会员 30代理 40平台)',
  `type` tinyint(3) unsigned NOT NULL DEFAULT '10' COMMENT '类型(10余额 20现金 30微信 90积分)',
  `mode` tinyint(3) unsigned NOT NULL DEFAULT '10' COMMENT '模式(10自助 20后台)',
  `action` tinyint(3) unsigned NOT NULL DEFAULT '10' COMMENT '方式(10充值 20扣减 30重置 40消费 50分红 60退款 70提现)',
  `order_no` varchar(20) NOT NULL DEFAULT '' COMMENT '订单号',
  `money` decimal(10,2) unsigned NOT NULL DEFAULT '0.00' COMMENT '操作数值',
  `gift_money` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '赠送数值',
  `remark` varchar(255) NOT NULL DEFAULT '' COMMENT '操作备注',
  `status` tinyint(3) unsigned NOT NULL DEFAULT '20' COMMENT '状态(10处理中 20已完成)',
  `user_id` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '用户ID',
  `shop_id` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '门店id',
  `applet_id` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '小程序id',
  `create_time` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '创建时间',
  `update_time` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '更新时间',
  PRIMARY KEY (`record_id`)
) ENGINE=InnoDB AUTO_INCREMENT=10001 DEFAULT CHARSET=utf8 COMMENT='交易记录表';

-- ----------------------------
-- 站点配置表
-- ----------------------------
DROP TABLE IF EXISTS `hema_setting`;
CREATE TABLE `hema_setting` (
  `key` varchar(30) NOT NULL COMMENT '设置项标示',
  `describe` varchar(255) NOT NULL DEFAULT '' COMMENT '设置项描述',
  `values` mediumtext NOT NULL COMMENT '设置内容（json格式）',
  `applet_id` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '小程序id',
  `update_time` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '更新时间',
  UNIQUE KEY `unique_key` (`key`,`applet_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='站点配置表';




-- ----------------------------
-- 分账账户关联表
-- ----------------------------
DROP TABLE IF EXISTS `hema_divide_account`;
CREATE TABLE `hema_divide_account` (
  `divide_account_id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT '主键ID',
  `open_id` varchar(100) NOT NULL DEFAULT '' COMMENT 'openid(唯一标识)',
  `user_id` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '用户id',
  `agent_id` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '所属代理',
  `applet_id` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '小程序id',
  `create_time` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '创建时间',
  `update_time` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '更新时间',
  PRIMARY KEY (`divide_account_id`)
) ENGINE=InnoDB AUTO_INCREMENT=10001 DEFAULT CHARSET=utf8 COMMENT='分账账户关联表';

-- ----------------------------
-- 站点配置
-- ----------------------------
DROP TABLE IF EXISTS `hema_config`;
CREATE TABLE `hema_config` (
  `config_id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT '站点配置ID',
  `user_name` varchar(255) NOT NULL DEFAULT '' COMMENT '用户名',
  `password` varchar(255) NOT NULL DEFAULT '' COMMENT '登录密码',
  `app_id` varchar(50) NOT NULL DEFAULT '' COMMENT 'AppID',
  `app_secret` varchar(50) NOT NULL DEFAULT '' COMMENT 'AppSecret',
  `encoding_aes_key` varchar(50) NOT NULL DEFAULT '' COMMENT 'encoding_aes_key',
  `token` varchar(50) NOT NULL DEFAULT '' COMMENT 'token',
  `api_domain` varchar(255) NOT NULL DEFAULT '' COMMENT '服务器域名',
  `authorize_domain` varchar(255) NOT NULL DEFAULT '' COMMENT '授权发起页域名',
  `component_verify_ticket` varchar(255) NOT NULL DEFAULT '' COMMENT 'ticket',
  `component_access_token` varchar(255) NOT NULL DEFAULT '' COMMENT '令牌',
  `expires_in` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '令牌过期时间',
  `create_time` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '创建时间',
  `update_time` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '更新时间',
  PRIMARY KEY (`config_id`)
) ENGINE=InnoDB AUTO_INCREMENT=10002 DEFAULT CHARSET=utf8 COMMENT='站点配置';

-- ----------------------------
-- 友情链接
-- ----------------------------
DROP TABLE IF EXISTS `hema_link`;
CREATE TABLE `hema_link` (
  `link_id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT '主键ID',
  `name` varchar(50) NOT NULL DEFAULT '' COMMENT '站点名称',
  `url` varchar(255) NOT NULL DEFAULT '' COMMENT '站点地址',
  `sort` int(11) unsigned NOT NULL DEFAULT '100' COMMENT '排序方式(数字越小越靠前)',
  `create_time` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '创建时间',
  `update_time` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '更新时间',
  PRIMARY KEY (`link_id`)
) ENGINE=InnoDB AUTO_INCREMENT=10005 DEFAULT CHARSET=utf8 COMMENT='友情链接表';

-- ----------------------------
-- 微信小程序表
-- ----------------------------
DROP TABLE IF EXISTS `hema_applet`;
CREATE TABLE `hema_applet` (
  `applet_id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT '小程序id',
  `password` varchar(255) NOT NULL DEFAULT '' COMMENT '登录密码',
  `app_type` varchar(50) NOT NULL DEFAULT '' COMMENT '应用类型',
  `shop_mode` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT '门店模式（10单门店 20多门店）',
  `source` tinyint(3) unsigned NOT NULL DEFAULT '10' COMMENT '账号来源，10=自助注册，20=平台注册',
  `app_name` varchar(255) NOT NULL DEFAULT '' COMMENT '小程序名称',
  `head_img` varchar(255) NOT NULL DEFAULT '' COMMENT '小程序头像',
  `qrcode_url` varchar(255) NOT NULL DEFAULT '' COMMENT '小程序二维码',
  `user_name` varchar(50) NOT NULL DEFAULT '' COMMENT '原始ID',
  `app_id` varchar(50) NOT NULL DEFAULT '' COMMENT '小程序AppID',
  `principal_name` varchar(255) NOT NULL DEFAULT '' COMMENT '主体名称',
  `api_domain` varchar(255) NOT NULL DEFAULT '' COMMENT '服务器域名',
  `webview_domain` varchar(255) NOT NULL DEFAULT '' COMMENT '业务域名',
  `signature` varchar(255) NOT NULL DEFAULT '' COMMENT '功能介绍',
  `access_token` varchar(255) NOT NULL DEFAULT '' COMMENT '令牌凭证',
  `expires_in` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '令牌到期时间',
  `authorizer_refresh_token` varchar(255) NOT NULL DEFAULT '' COMMENT '刷新令牌',
  `is_live` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT '是否开启直播插件 1开启，0关闭',
  `status` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT '是否授权(0否 1是)',
  `user_id` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '用户ID',
  `agent_id` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '所属代理',
  `expire_time` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '小程序到期时间',
  `create_time` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '创建时间',
  `update_time` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '更新时间',
  PRIMARY KEY (`applet_id`)
) ENGINE=InnoDB AUTO_INCREMENT=10001 DEFAULT CHARSET=utf8 COMMENT='微信小程序表';

-- ----------------------------
-- 小程序模板代码表
-- ----------------------------
DROP TABLE IF EXISTS `hema_template_code`;
CREATE TABLE `hema_template_code` (
  `template_code_id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT '模板ID',
  `app_type` varchar(50) NOT NULL DEFAULT '' COMMENT '模板类型',
  `id` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '服务端ID',
  `user_version` varchar(255) NOT NULL DEFAULT '' COMMENT '版本号',
  `user_desc` varchar(255) NOT NULL DEFAULT '' COMMENT '版本描述',
  `create_time` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '创建时间',
  `update_time` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '更新时间',
  PRIMARY KEY (`template_code_id`)
) ENGINE=InnoDB AUTO_INCREMENT=10001 DEFAULT CHARSET=utf8 COMMENT='小程序模板代码表';

-- ----------------------------
-- 小程序名称表
-- ----------------------------
DROP TABLE IF EXISTS `hema_applet_name`;
CREATE TABLE `hema_applet_name` (
  `applet_name_id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT '主键',
  `nick_name` varchar(50) NOT NULL DEFAULT '' COMMENT '名称',
  `audit_id` varchar(50) NOT NULL DEFAULT '' COMMENT '审核编号',
  `status` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT '状态(0审核中，1设置成功 ，2被驳回)',
  `reason` varchar(255) NOT NULL DEFAULT '' COMMENT '驳回原因',
  `applet_id` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '小程序id',
  `create_time` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '创建时间',
  `update_time` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '更新时间',
  PRIMARY KEY (`applet_name_id`)
) ENGINE=InnoDB AUTO_INCREMENT=10001 DEFAULT CHARSET=utf8 COMMENT='小程序名称表';

-- ----------------------------
-- 公众号表
-- ----------------------------
DROP TABLE IF EXISTS `hema_wechat`;
CREATE TABLE `hema_wechat` (
  `wechat_id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT '编号id',
  `app_name` varchar(50) NOT NULL DEFAULT '' COMMENT '名称',
  `head_img` varchar(255) NOT NULL DEFAULT '' COMMENT '头像',
  `qrcode_url` varchar(255) NOT NULL DEFAULT '' COMMENT '二维码',
  `user_name` varchar(50) NOT NULL DEFAULT '' COMMENT '原始ID',
  `app_id` varchar(50) NOT NULL DEFAULT '' COMMENT 'AppID',
  `principal_name` varchar(255) NOT NULL DEFAULT '' COMMENT '主体名称',
  `access_token` varchar(255) NOT NULL DEFAULT '' COMMENT '令牌凭证',
  `expires_in` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '令牌到期时间',
  `authorizer_refresh_token` varchar(255) NOT NULL DEFAULT '' COMMENT '刷新令牌',
  `status` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT '是否授权(0否 1是)',
  `applet_id` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '小程序id',
  `create_time` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '创建时间',
  `update_time` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '更新时间',
  PRIMARY KEY (`wechat_id`)
) ENGINE=InnoDB AUTO_INCREMENT=10001 DEFAULT CHARSET=utf8 COMMENT='公众号表';

-- ----------------------------
-- 微信批量发送信息表
-- ----------------------------
DROP TABLE IF EXISTS `hema_wechat_batch_send`;
CREATE TABLE `hema_wechat_batch_send` (
  `wechat_batch_send_id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT '编号ID',
  `msg_id` varchar(50) NOT NULL DEFAULT '' COMMENT '任务ID',
  `title` varchar(255) NOT NULL DEFAULT '' COMMENT '消息标题',
  `msg_type` varchar(20) NOT NULL DEFAULT '' COMMENT '消息类型',
  `content` varchar(255) NOT NULL DEFAULT '' COMMENT '消息内容',
  `recommend` varchar(255) NOT NULL DEFAULT '' COMMENT '推荐语',
  `need_open_comment` tinyint(3) unsigned NOT NULL DEFAULT '1' COMMENT '是否开启评论，0关闭，1开启',
  `only_fans_can_comment` tinyint(3) unsigned NOT NULL DEFAULT '1' COMMENT '谁可评论，0所有人，1仅粉丝',
  `send_ignore_reprint` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT '为转载时，0停止群发，1继续群发',
  `description` varchar(255) NOT NULL DEFAULT '' COMMENT '内容描述',
  `status` int(11) unsigned NOT NULL DEFAULT '10' COMMENT '群发状态',
  `fans_count` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '群发人数',
  `send_count` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '成功人数',
  `applet_id` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '小程序id',
  `create_time` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '创建时间',
  `update_time` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '更新时间',
  PRIMARY KEY (`wechat_batch_send_id`)
) ENGINE=InnoDB AUTO_INCREMENT=10001 DEFAULT CHARSET=utf8 COMMENT='微信批量发送信息表';

-- ----------------------------
-- 公众号素材表
-- ----------------------------
DROP TABLE IF EXISTS `hema_material`;
CREATE TABLE `hema_material` (
  `material_id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT '文件id',
  `name` varchar(50) NOT NULL DEFAULT '' COMMENT '素材名称',
  `file_type` tinyint(3) unsigned NOT NULL DEFAULT '10' COMMENT '文件类型 10=图片，20=音频，30=视频，40=图文',
  `file_path` varchar(255) NOT NULL DEFAULT '' COMMENT '本地文件名称',
  `media_id` varchar(255) NOT NULL DEFAULT '' COMMENT '素材media_id',
  `url` varchar(255) NOT NULL DEFAULT '' COMMENT '素材网络地址',
  `text_no` varchar(20) NOT NULL DEFAULT '' COMMENT '图文素材编号',
  `introduction` varchar(255) NOT NULL DEFAULT '' COMMENT '视频素材描述',
  `applet_id` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '小程序id',
  `create_time` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '创建时间',
  `update_time` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '更新时间',
  PRIMARY KEY (`material_id`)
) ENGINE=InnoDB AUTO_INCREMENT=10001 DEFAULT CHARSET=utf8 COMMENT='公众号素材表';

-- ----------------------------
-- 公众号图文素材详情表
-- ----------------------------
DROP TABLE IF EXISTS `hema_material_text`;
CREATE TABLE `hema_material_text` (
  `material_text_id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT '记录编号',
  `text_no` varchar(20) NOT NULL DEFAULT '' COMMENT '素材编号',
  `id` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT '素材信息序号',
  `title` varchar(100) NOT NULL DEFAULT '' COMMENT '标题',
  `author` varchar(50) NOT NULL DEFAULT '' COMMENT '作者',
  `digest` varchar(100) NOT NULL DEFAULT '' COMMENT '摘要',
  `content` longtext NOT NULL COMMENT '正文',
  `media_id` varchar(255) NOT NULL DEFAULT '' COMMENT '素材media_id',
  `file_path` varchar(255) NOT NULL DEFAULT '' COMMENT '本地文件名称',
  `url` varchar(255) NOT NULL DEFAULT '' COMMENT '封面网络地址',
  `content_source_url` varchar(255) NOT NULL DEFAULT '#' COMMENT '图文链接地址',
  `create_time` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '创建时间',
  `update_time` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '更新时间',
  PRIMARY KEY (`material_text_id`)
) ENGINE=InnoDB AUTO_INCREMENT=10001 DEFAULT CHARSET=utf8 COMMENT='公众号图文素材详情表';

-- ----------------------------
-- 公众号关键字回复
-- ----------------------------
DROP TABLE IF EXISTS `hema_keyword`;
CREATE TABLE `hema_keyword` (
  `keyword_id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT '编号ID',
  `keyword` varchar(50) NOT NULL DEFAULT '' COMMENT '关键字',
  `type` varchar(10) NOT NULL DEFAULT 'text' COMMENT '消息类型 text=文字，image=图片，voice=音频，video=视频，music=音乐，news=图文',
  `is_open` tinyint(3) unsigned NOT NULL DEFAULT '1' COMMENT '是否开启 0=关闭，1=开启',
  `content` longtext COMMENT '消息内容',
  `applet_id` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '小程序id',
  `create_time` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '创建时间',
  `update_time` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '更新时间',
  PRIMARY KEY (`keyword_id`)
) ENGINE=InnoDB AUTO_INCREMENT=10001 DEFAULT CHARSET=utf8 COMMENT='公众号关键字回复';

-- ----------------------------
-- 商户详情表
-- ----------------------------
DROP TABLE IF EXISTS `hema_user_detail`;
CREATE TABLE `hema_user_detail` (
  `user_detail_id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT '主键ID',
  `mch_id` varchar(50) NOT NULL DEFAULT '' COMMENT '商户号',
  `merchant_shortname` varchar(50) NOT NULL DEFAULT '' COMMENT '商户简称',
  `id_card_copy` varchar(100) NOT NULL DEFAULT '' COMMENT '身份证正面',
  `id_card_national` varchar(100) NOT NULL DEFAULT '' COMMENT '身份证反面',
  `id_card_name` varchar(50) NOT NULL DEFAULT '' COMMENT '证件姓名',
  `id_card_number` varchar(50) NOT NULL DEFAULT '' COMMENT '证件号码',
  `id_card_address` varchar(255) NOT NULL DEFAULT '' COMMENT '居住地址',
  `card_period_begin` varchar(50) NOT NULL DEFAULT '' COMMENT '身份证有效期开始时间',
  `card_period_end` varchar(50) NOT NULL DEFAULT '' COMMENT '身份证有效期结束时间',
  `biz_address_code` varchar(50) NOT NULL DEFAULT '' COMMENT '门店所在省市编码',
  `biz_store_address` varchar(255) NOT NULL DEFAULT '' COMMENT '门店地址',
  `store_entrance_pic` varchar(255) NOT NULL DEFAULT '' COMMENT '门头照',
  `indoor_pic` varchar(255) NOT NULL DEFAULT '' COMMENT '店内照片',
  `legal_persona_wechat` varchar(50) NOT NULL DEFAULT '' COMMENT '微信号',
  `mobile_phone` varchar(50) NOT NULL DEFAULT '' COMMENT '手机号',
  `contact_email` varchar(50) NOT NULL DEFAULT '' COMMENT '邮箱',
  `subject_type` varchar(50) NOT NULL DEFAULT 'SUBJECT_TYPE_INDIVIDUAL' COMMENT '主体类型',
  `license_copy` varchar(100) NOT NULL DEFAULT '' COMMENT '营业执照',
  `merchant_name` varchar(50) NOT NULL DEFAULT '' COMMENT '营业执照名称',
  `license_number` varchar(50) NOT NULL DEFAULT '' COMMENT '统一社会信用代码',
  `qualifications` varchar(255) NOT NULL DEFAULT '' COMMENT '特殊资质',
  `account_bank` varchar(100) NOT NULL DEFAULT '' COMMENT '开户银行',
  `bank_address_code` varchar(50) NOT NULL DEFAULT '' COMMENT '开户银行所在省市编码',
  `account_number` varchar(50) NOT NULL DEFAULT '' COMMENT '银行账号',
  `user_id` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '用户ID',
  `create_time` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '创建时间',
  `update_time` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '更新时间',
  PRIMARY KEY (`user_detail_id`),
  UNIQUE KEY `user_id` (`user_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=10001 DEFAULT CHARSET=utf8 COMMENT='商户详情表';


-- ----------------------------
-- 认证申请表
-- ----------------------------
DROP TABLE IF EXISTS `hema_apply`;
CREATE TABLE `hema_apply` (
  `apply_id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT '主键ID',
  `business_code` varchar(50) NOT NULL DEFAULT '' COMMENT '自定义申请编号',
  `applyment_id` varchar(50) NOT NULL DEFAULT '' COMMENT '微信申请编号',
  `apply_mode` tinyint(3) unsigned NOT NULL DEFAULT '10' COMMENT '认证类型(10小程序申请 20商户入驻 30支付认证 40代理认证 50实名认证)',
  `user_id` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '申请人ID',
  `applet_id` varchar(50) NOT NULL DEFAULT '' COMMENT '小程序ID',
  `app_id` varchar(50) NOT NULL DEFAULT '' COMMENT '小程序AppID',
  `auth_mode` tinyint(3) unsigned NOT NULL DEFAULT '10' COMMENT '认证类型(10工商 20个人)',
  `agent_mode` tinyint(3) unsigned NOT NULL DEFAULT '10' COMMENT '代理级别(10 区县 20 市级 30 省级)',
  `location` varchar(50) NOT NULL DEFAULT '' COMMENT '位置坐标',
  `province` varchar(20) NOT NULL DEFAULT '' COMMENT '所在省份',
  `city` varchar(20) NOT NULL DEFAULT '' COMMENT '所在城市',
  `district` varchar(20) NOT NULL DEFAULT '' COMMENT '所在区/县',
  `pay_status` tinyint(3) unsigned NOT NULL DEFAULT '10' COMMENT '支付状态(10待支付，20已支付，30免审核费)',
  `pay_time` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '支付时间',
  `reject` varchar(255) NOT NULL DEFAULT '' COMMENT '驳回原因',
  `apply_status` tinyint(3) unsigned NOT NULL DEFAULT '10' COMMENT '审核状态(10待审核，20验证中，30已审核，40已驳回)',
  `applyment_state` tinyint(3) unsigned NOT NULL DEFAULT '10' COMMENT '官方平台申请状态',
  `apply_time` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '审查时间',
  `agent_id` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '所属代理',
  `create_time` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '创建时间',
  `update_time` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '更新时间',
  PRIMARY KEY (`apply_id`)
) ENGINE=InnoDB AUTO_INCREMENT=10001 DEFAULT CHARSET=utf8 COMMENT='认证申请表';

-- ----------------------------
-- 小程序模板市场表
-- ----------------------------
DROP TABLE IF EXISTS `hema_template`;
CREATE TABLE `hema_template` (
  `template_id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT '主键ID',
  `name` varchar(50) NOT NULL DEFAULT '' COMMENT '版本名称',
  `app_type` varchar(50) NOT NULL DEFAULT '' COMMENT '模板类型',
  `is_many` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT '支持多门店(1支持，0不支持)',
  `buy_single` varchar(255) NOT NULL DEFAULT '' COMMENT '单门店购买价格',
  `renew_single` varchar(255) NOT NULL DEFAULT '' COMMENT '单门店续费价格',
  `buy_many` varchar(255) NOT NULL DEFAULT '' COMMENT '多门店购买价格',
  `renew_many` varchar(255) NOT NULL DEFAULT '' COMMENT '多门店续费价格',
  `trial_day` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT '免费试用天数',
  `detail` varchar(255) NOT NULL DEFAULT '' COMMENT '备注说明',
  `single_price` decimal(10,2) unsigned NOT NULL DEFAULT '0.00' COMMENT '单门店代理价',
  `many_price` decimal(10,2) unsigned NOT NULL DEFAULT '0.00' COMMENT '多门店代理价',
  `status` tinyint(3) unsigned NOT NULL DEFAULT '10' COMMENT '上架状态(10上架 20下架)',
  `agent_id` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '所属代理',
  `sort` int(11) unsigned NOT NULL DEFAULT '100' COMMENT '排序方式(数字越小越靠前)',
  `create_time` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '创建时间',
  `update_time` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '更新时间',
  PRIMARY KEY (`template_id`)
) ENGINE=InnoDB AUTO_INCREMENT=10001 DEFAULT CHARSET=utf8 COMMENT='小程序模板市场表';

