INSERT INTO `hema_config` VALUES (10001,'admin','7d95a09138d667b108b1252fea5ae15d','','','','','','','','','0','1529926348','1529926348');
INSERT INTO `hema_user` VALUES (10001,'','','test','a4839e2b0a5b683a32f15ae7e99c8544','','',0,'中国','江苏省','徐州市',0,'','',0,0,0,0,'',0,0,20,10,0,0,0,0,1580703326,1580744990);
INSERT INTO `hema_link` VALUES (10001,'河马云店','https://www.hemaphp.com/',100,1580703326,1580744990);
INSERT INTO `hema_link` VALUES (10002,'ThinkPHP','http://www.thinkphp.cn/',100,1580703326,1580744990);
INSERT INTO `hema_link` VALUES (10003,'码云Gitee','https://gitee.com/',100,1580745030,1580745030);
INSERT INTO `hema_link` VALUES (10004,'腾讯云','https://cloud.tencent.com/',100,1580745067,1580745067);

-- ----------------------------
-- 用户
-- ----------------------------
CREATE TABLE IF NOT EXISTS `hema_sharewifi_user` (
  `user_id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT '主键ID',
  `union_id` varchar(50) NOT NULL DEFAULT '' COMMENT '平台唯一标识',
  `open_id` varchar(50) NOT NULL DEFAULT '' COMMENT 'openid(唯一标识)',
  `nickname` varchar(255) NOT NULL DEFAULT '' COMMENT '昵称',
  `avatar` varchar(255) NOT NULL DEFAULT '' COMMENT '头像',
  `gender` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT '性别',
  `province` varchar(50) NOT NULL DEFAULT '' COMMENT '省份',
  `city` varchar(50) NOT NULL DEFAULT '' COMMENT '城市',
  `district` varchar(50) NOT NULL DEFAULT '' COMMENT '区县',
  `address` varchar(255) NOT NULL DEFAULT '' COMMENT '社区门牌',
  `location` varchar(50) NOT NULL DEFAULT '' COMMENT '位置坐标',
  `linkman` varchar(20) NOT NULL DEFAULT '' COMMENT '联系人',
  `phone` varchar(20) NOT NULL DEFAULT '' COMMENT '联系电话',
  `money` decimal(10,2) unsigned NOT NULL DEFAULT '0.00' COMMENT '余额',
  `score` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '积分',
  `status` tinyint(3) unsigned NOT NULL DEFAULT '10' COMMENT '身份(10普通用户 20区域团长)',
  `recommender` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '推荐人',
  `login_count` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '登录次数',
  `create_time` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '创建时间',
  `update_time` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '更新时间',
  PRIMARY KEY (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=10001 DEFAULT CHARSET=utf8 COMMENT='用户表';
-- ----------------------------
-- 门店分类表
-- ----------------------------
CREATE TABLE IF NOT EXISTS `hema_sharewifi_category` (
  `category_id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT '主键ID',
  `name` varchar(50) NOT NULL DEFAULT '' COMMENT '分类名称',
  `logo` varchar(100) NOT NULL DEFAULT '' COMMENT '分类图标',
  `sort` int(11) unsigned NOT NULL DEFAULT '100' COMMENT '排序方式(数字越小越靠前)',
  `create_time` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '创建时间',
  `update_time` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '更新时间',
  PRIMARY KEY (`category_id`)
) ENGINE=InnoDB AUTO_INCREMENT=10001 DEFAULT CHARSET=utf8 COMMENT='门店分类表';
-- ----------------------------
-- 门店表
-- ----------------------------
CREATE TABLE IF NOT EXISTS `hema_sharewifi_shop` (
  `shop_id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT '主键ID',
  `category_id` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '类别',
  `name` varchar(50) NOT NULL DEFAULT '' COMMENT '名称',
  `logo` varchar(255) NOT NULL DEFAULT '' COMMENT 'logo',
  `linkman` varchar(20) NOT NULL DEFAULT '' COMMENT '联系人',
  `phone` varchar(20) NOT NULL DEFAULT '' COMMENT '联系电话',
  `shop_hours` varchar(20) NOT NULL DEFAULT '' COMMENT '营业时间',
  `province` varchar(20) NOT NULL DEFAULT '' COMMENT '所在省份',
  `city` varchar(20) NOT NULL DEFAULT '' COMMENT '所在城市',
  `district` varchar(20) NOT NULL DEFAULT '' COMMENT '所在区/县',
  `detail` varchar(255) NOT NULL DEFAULT '' COMMENT '详细地址',
  `location` varchar(100) NOT NULL DEFAULT '' COMMENT '门店坐标',
  `summary` varchar(255) NOT NULL DEFAULT '' COMMENT '门店简介',
  `ss_id` varchar(50) NOT NULL DEFAULT '' COMMENT 'WIFI账号',
  `ss_key` varchar(50) NOT NULL DEFAULT '' COMMENT 'WIFI密码',
  `status` tinyint(3) unsigned NOT NULL DEFAULT '1' COMMENT '状态(1开启 0关闭)',
  `sort` int(11) unsigned NOT NULL DEFAULT '100' COMMENT '排序方式(数字越小越靠前)',
  `visit` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '访问量',
  `user_id` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '用户ID',
  `create_time` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '创建时间',
  `update_time` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '更新时间',
  PRIMARY KEY (`shop_id`)
) ENGINE=InnoDB AUTO_INCREMENT=10001 DEFAULT CHARSET=utf8 COMMENT='门店表';
-- ----------------------------
-- 配置表
-- ----------------------------
CREATE TABLE IF NOT EXISTS `hema_sharewifi_setting` (
  `key` varchar(30) NOT NULL COMMENT '设置项标示',
  `describe` varchar(255) NOT NULL DEFAULT '' COMMENT '设置项描述',
  `values` mediumtext NOT NULL COMMENT '设置内容（json格式）',
  `update_time` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '更新时间',
  UNIQUE KEY `unique_key` (`key`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='配置表';
-- ----------------------------
-- 帮助文档
-- ----------------------------
CREATE TABLE IF NOT EXISTS `hema_sharewifi_help` (
  `help_id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT '主键ID',
  `title` varchar(255) NOT NULL DEFAULT '' COMMENT '帮助标题',
  `content` text NOT NULL COMMENT '帮助内容',
  `sort` int(11) unsigned NOT NULL DEFAULT '100' COMMENT '排序(数字越小越靠前)',
  `create_time` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '创建时间',
  `update_time` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '更新时间',
  PRIMARY KEY (`help_id`)
) ENGINE=InnoDB AUTO_INCREMENT=10001 DEFAULT CHARSET=utf8 COMMENT='帮助文档表';
-- ----------------------------
-- 团长申请表
-- ----------------------------
CREATE TABLE IF NOT EXISTS `hema_sharewifi_apply` (
  `apply_id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT '主键ID',
  `reason` varchar(255) NOT NULL DEFAULT '' COMMENT '驳回原因',
  `status` tinyint(3) unsigned NOT NULL DEFAULT '10' COMMENT '状态(10审核中 20被驳回 30 已通过)',
  `user_id` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '用户ID',
  `create_time` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '创建时间',
  `update_time` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '更新时间',
  PRIMARY KEY (`apply_id`)
) ENGINE=InnoDB AUTO_INCREMENT=10001 DEFAULT CHARSET=utf8 COMMENT='团长申请表';
-- ----------------------------
-- 连接记录表
-- ----------------------------
CREATE TABLE IF NOT EXISTS `hema_sharewifi_connect` (
  `connect_id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT '主键ID',
  `is_success` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT '状态(0失败 1成功)',
  `reason` varchar(50) NOT NULL DEFAULT '' COMMENT '原因',
  `shop_id` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '门店ID',
  `user_id` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '用户ID',
  `create_time` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '创建时间',
  `update_time` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '更新时间',
  PRIMARY KEY (`connect_id`)
) ENGINE=InnoDB AUTO_INCREMENT=10001 DEFAULT CHARSET=utf8 COMMENT='连接记录表';
-- ----------------------------
-- 用户钱包记录表
-- ----------------------------
CREATE TABLE IF NOT EXISTS `hema_sharewifi_wallet` (
  `wallet_id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT '主键ID',
  `mode` tinyint(3) unsigned NOT NULL DEFAULT '10' COMMENT '模式(10收入 20提现)',
  `money` decimal(10,2) unsigned NOT NULL DEFAULT '0.00' COMMENT '金额',
  `reason` varchar(50) NOT NULL DEFAULT '' COMMENT '原因',
  `status` tinyint(3) unsigned NOT NULL DEFAULT '10' COMMENT '状态(10审核中 20被驳回 30 打款中 40 已打款)',
  `user_id` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '用户ID',
  `create_time` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '创建时间',
  `update_time` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '更新时间',
  PRIMARY KEY (`wallet_id`)
) ENGINE=InnoDB AUTO_INCREMENT=10001 DEFAULT CHARSET=utf8 COMMENT='用户钱包记录表';
-- ----------------------------
-- 广告记录表
-- ----------------------------
CREATE TABLE IF NOT EXISTS `hema_sharewifi_advert` (
  `advert_id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT '主键ID',
  `key` varchar(100) NOT NULL DEFAULT '' COMMENT '广告位',
  `show` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '曝光量',
  `click` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '点击量',
  `user_id` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '用户ID',
  `create_time` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '创建时间',
  `update_time` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '更新时间',
  PRIMARY KEY (`advert_id`)
) ENGINE=InnoDB AUTO_INCREMENT=10001 DEFAULT CHARSET=utf8 COMMENT='广告记录表';
-- ----------------------------
-- 图库表
-- ----------------------------
DROP TABLE IF EXISTS `hema_upload_file`;
CREATE TABLE IF NOT EXISTS `hema_upload_file` (
  `file_id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT '文件id',
  `storage` varchar(20) NOT NULL DEFAULT '' COMMENT '存储方式',
  `group_id` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '文件分组id',
  `domain` varchar(255) NOT NULL DEFAULT '' COMMENT '存储域名',
  `file_path` varchar(255) NOT NULL DEFAULT '' COMMENT '文件路径',
  `file_size` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '文件大小(字节)',
  `file_type` varchar(20) NOT NULL DEFAULT '' COMMENT '文件类型',
  `file_ext` varchar(20) NOT NULL DEFAULT '' COMMENT '文件扩展名',
  `is_delete` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT '软删除',
  `applet_id` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '小程序id',
  `create_time` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '创建时间',
  PRIMARY KEY (`file_id`)
) ENGINE=InnoDB AUTO_INCREMENT=10001 DEFAULT CHARSET=utf8 COMMENT='图库表';

-- ----------------------------
-- 图库分组表
-- ----------------------------
DROP TABLE IF EXISTS `hema_upload_group`;
CREATE TABLE IF NOT EXISTS `hema_upload_group` (
  `group_id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT '分类id',
  `group_type` varchar(10) NOT NULL DEFAULT '' COMMENT '文件类型',
  `group_name` varchar(30) NOT NULL DEFAULT '' COMMENT '分类名称',
  `sort` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '分类排序(数字越小越靠前)',
  `applet_id` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '小程序id',
  `create_time` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '创建时间',
  `update_time` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '更新时间',
  PRIMARY KEY (`group_id`),
  KEY `type_index` (`group_type`)
) ENGINE=InnoDB AUTO_INCREMENT=10001 DEFAULT CHARSET=utf8 COMMENT='图库分组表';