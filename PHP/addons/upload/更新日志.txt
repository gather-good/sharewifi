### 图库管理 - 图片文件上传与管理 ###
--------------------------------
### v1.0.4 更新日志 ###
修复：部分操作无效
优化：弹窗界面样式
--------------------------------
### v1.0.3 更新日志 ###
优化：弹窗功能选项
--------------------------------
### v1.0.2 更新日志 ###
优化：插件文件结构
--------------------------------
### v1.0.1 更新日志 ###
新增：存储方式设置
--------------------------------