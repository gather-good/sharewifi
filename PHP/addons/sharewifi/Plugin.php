<?php
namespace addons\sharewifi;

use think\Addons;

/**
 * 共享WIFI插件
 */
class Plugin extends Addons
{
    // 该插件的基础信息
    public $info = [
        'name' => 'sharewifi',	    // 插件标识
        'title' => '共享WIFI',	// 插件名称
        'description' => '拿下一座城，躺赚一辈子',	// 插件简介
        'status' => 1,	// 状态
        'author' => 'hemaPHP',
        'version' => '1.0.1'
    ];
     /**
     * 插件安装方法
     * @return bool
     */
    public function install()
    {
        return true;
    }
    /**
     * 插件卸载方法
     * @return bool
     */
    public function uninstall()
    {
        return true;
    }
    /**
     * 插件升级方法
     * @return bool
     */
    public function upgrade()
    {
        return true;
    }
    /**
     * 插件启用方法
     * @return bool
     */
    public function enable()
    {
        return true;
    }
    /**
     * 插件禁用方法
     * @return bool
     */
    public function disable()
    {
        return true;
    }
}