<?php
/** 
 * 配置文件 
 */
return [    
    [        
        'name' => 'app_name',//参数名称        
        'title' => '小程序名称：', //显示标题        
        'type' => 'text', //表单元素类型 - 文本框        
        //属性        
        'attribute' => [             
            'placeholder' => '微信小程序的名称',            
            'maxlength' => '150',//可输入最大长度            
            'required' => true,//是否必填            
            'disabled' => false,//是否禁用        
        ],        
        'msg' => '',//提示内容        
        'value' => '',//初始值    
    ],
	[
	    'name' => 'app_id',//参数名称
	    'title' => 'APP_ID：', //显示标题
	    'type' => 'text', //表单元素类型 - 文本框
	    //属性
	    'attribute' => [ 
	        'placeholder' => '微信小程序的APP_ID',
	        'maxlength' => '150',//可输入最大长度
	        'required' => true,//是否必填
	        'disabled' => false,//是否禁用
	    ],
	    'msg' => '',//提示内容
	    'value' => '',//初始值
	],
	[
	    'name' => 'app_secret',//参数名称
	    'title' => 'APP_SECRET：', //显示标题
	    'type' => 'text', //表单元素类型 - 文本框
	    //属性
	    'attribute' => [ 
	        'placeholder' => '微信小程序的APP_SECRET',
	        'maxlength' => '150',//可输入最大长度
	        'required' => true,//是否必填
	        'disabled' => false,//是否禁用
	    ],
	    'msg' => '',//提示内容
	    'value' => '',//初始值
	],
	[
	    'name' => 'copyright',//参数名称
	    'title' => '显示版权：', //显示标题
	    'type' => 'text', //表单元素类型 - 文本框
	    //属性
	    'attribute' => [ 
	        'placeholder' => '小程序底部显示的版权信息',
	        'maxlength' => '150',//可输入最大长度
	        'required' => true,//是否必填
	        'disabled' => false,//是否禁用
	    ],
	    'msg' => '',//提示内容
	    'value' => '',//初始值
	],	
];