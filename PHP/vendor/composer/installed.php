<?php return array(
    'root' => array(
        'name' => 'topthink/think',
        'pretty_version' => '1.0.0+no-version-set',
        'version' => '1.0.0.0',
        'reference' => NULL,
        'type' => 'project',
        'install_path' => __DIR__ . '/../../',
        'aliases' => array(),
        'dev' => true,
    ),
    'versions' => array(
        'aliyuncs/oss-sdk-php' => array(
            'pretty_version' => 'v2.4.3',
            'version' => '2.4.3.0',
            'reference' => '4ccead614915ee6685bf30016afb01aabd347e46',
            'type' => 'library',
            'install_path' => __DIR__ . '/../aliyuncs/oss-sdk-php',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'bacon/bacon-qr-code' => array(
            'pretty_version' => '2.0.7',
            'version' => '2.0.7.0',
            'reference' => 'd70c840f68657ce49094b8d91f9ee0cc07fbf66c',
            'type' => 'library',
            'install_path' => __DIR__ . '/../bacon/bacon-qr-code',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'dasprid/enum' => array(
            'pretty_version' => '1.0.3',
            'version' => '1.0.3.0',
            'reference' => '5abf82f213618696dda8e3bf6f64dd042d8542b2',
            'type' => 'library',
            'install_path' => __DIR__ . '/../dasprid/enum',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'endroid/qr-code' => array(
            'pretty_version' => '4.4.9',
            'version' => '4.4.9.0',
            'reference' => 'bf087fa1e93a1b7310e2d94d187e26ae51db199d',
            'type' => 'library',
            'install_path' => __DIR__ . '/../endroid/qr-code',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'ezyang/htmlpurifier' => array(
            'pretty_version' => 'v4.14.0',
            'version' => '4.14.0.0',
            'reference' => '12ab42bd6e742c70c0a52f7b82477fcd44e64b75',
            'type' => 'library',
            'install_path' => __DIR__ . '/../ezyang/htmlpurifier',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'guzzlehttp/command' => array(
            'pretty_version' => '1.2.1',
            'version' => '1.2.1.0',
            'reference' => '04b06e7f5ef37d814aeb3f4b6015b65a9d4412c5',
            'type' => 'library',
            'install_path' => __DIR__ . '/../guzzlehttp/command',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'guzzlehttp/guzzle' => array(
            'pretty_version' => '7.4.1',
            'version' => '7.4.1.0',
            'reference' => 'ee0a041b1760e6a53d2a39c8c34115adc2af2c79',
            'type' => 'library',
            'install_path' => __DIR__ . '/../guzzlehttp/guzzle',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'guzzlehttp/guzzle-services' => array(
            'pretty_version' => '1.3.1',
            'version' => '1.3.1.0',
            'reference' => '3731f120ce6856f4c71fff7cb2a27e263fe69f84',
            'type' => 'library',
            'install_path' => __DIR__ . '/../guzzlehttp/guzzle-services',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'guzzlehttp/promises' => array(
            'pretty_version' => '1.5.1',
            'version' => '1.5.1.0',
            'reference' => 'fe752aedc9fd8fcca3fe7ad05d419d32998a06da',
            'type' => 'library',
            'install_path' => __DIR__ . '/../guzzlehttp/promises',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'guzzlehttp/psr7' => array(
            'pretty_version' => '2.1.0',
            'version' => '2.1.0.0',
            'reference' => '089edd38f5b8abba6cb01567c2a8aaa47cec4c72',
            'type' => 'library',
            'install_path' => __DIR__ . '/../guzzlehttp/psr7',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'guzzlehttp/uri-template' => array(
            'pretty_version' => 'v1.0.1',
            'version' => '1.0.1.0',
            'reference' => 'b945d74a55a25a949158444f09ec0d3c120d69e2',
            'type' => 'library',
            'install_path' => __DIR__ . '/../guzzlehttp/uri-template',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'hemaphp/think-addons' => array(
            'pretty_version' => '2.0.5',
            'version' => '2.0.5.0',
            'reference' => '7eb740cb219a111d593a05ad88248a74f640fe5c',
            'type' => 'library',
            'install_path' => __DIR__ . '/../hemaphp/think-addons',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'league/flysystem' => array(
            'pretty_version' => '1.1.9',
            'version' => '1.1.9.0',
            'reference' => '094defdb4a7001845300334e7c1ee2335925ef99',
            'type' => 'library',
            'install_path' => __DIR__ . '/../league/flysystem',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'league/flysystem-cached-adapter' => array(
            'pretty_version' => '1.1.0',
            'version' => '1.1.0.0',
            'reference' => 'd1925efb2207ac4be3ad0c40b8277175f99ffaff',
            'type' => 'library',
            'install_path' => __DIR__ . '/../league/flysystem-cached-adapter',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'league/mime-type-detection' => array(
            'pretty_version' => '1.9.0',
            'version' => '1.9.0.0',
            'reference' => 'aa70e813a6ad3d1558fc927863d47309b4c23e69',
            'type' => 'library',
            'install_path' => __DIR__ . '/../league/mime-type-detection',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'maennchen/zipstream-php' => array(
            'pretty_version' => '2.1.0',
            'version' => '2.1.0.0',
            'reference' => 'c4c5803cc1f93df3d2448478ef79394a5981cc58',
            'type' => 'library',
            'install_path' => __DIR__ . '/../maennchen/zipstream-php',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'markbaker/complex' => array(
            'pretty_version' => '3.0.1',
            'version' => '3.0.1.0',
            'reference' => 'ab8bc271e404909db09ff2d5ffa1e538085c0f22',
            'type' => 'library',
            'install_path' => __DIR__ . '/../markbaker/complex',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'markbaker/matrix' => array(
            'pretty_version' => '3.0.0',
            'version' => '3.0.0.0',
            'reference' => 'c66aefcafb4f6c269510e9ac46b82619a904c576',
            'type' => 'library',
            'install_path' => __DIR__ . '/../markbaker/matrix',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'myclabs/php-enum' => array(
            'pretty_version' => '1.8.3',
            'version' => '1.8.3.0',
            'reference' => 'b942d263c641ddb5190929ff840c68f78713e937',
            'type' => 'library',
            'install_path' => __DIR__ . '/../myclabs/php-enum',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'nelexa/zip' => array(
            'pretty_version' => '4.0.1',
            'version' => '4.0.1.0',
            'reference' => 'a18f80db509b1b6e9798e2745dc100759107f50c',
            'type' => 'library',
            'install_path' => __DIR__ . '/../nelexa/zip',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'overtrue/easy-sms' => array(
            'pretty_version' => '2.2.0',
            'version' => '2.2.0.0',
            'reference' => 'fda1b6fcde861451ccf54e1071b4e1877455d89a',
            'type' => 'library',
            'install_path' => __DIR__ . '/../overtrue/easy-sms',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'phpoffice/phpspreadsheet' => array(
            'pretty_version' => '1.21.0',
            'version' => '1.21.0.0',
            'reference' => '1a359d2ccbb89c05f5dffb32711a95f4afc67964',
            'type' => 'library',
            'install_path' => __DIR__ . '/../phpoffice/phpspreadsheet',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'psr/cache' => array(
            'pretty_version' => '1.0.1',
            'version' => '1.0.1.0',
            'reference' => 'd11b50ad223250cf17b86e38383413f5a6764bf8',
            'type' => 'library',
            'install_path' => __DIR__ . '/../psr/cache',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'psr/container' => array(
            'pretty_version' => '1.1.2',
            'version' => '1.1.2.0',
            'reference' => '513e0666f7216c7459170d56df27dfcefe1689ea',
            'type' => 'library',
            'install_path' => __DIR__ . '/../psr/container',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'psr/http-client' => array(
            'pretty_version' => '1.0.1',
            'version' => '1.0.1.0',
            'reference' => '2dfb5f6c5eff0e91e20e913f8c5452ed95b86621',
            'type' => 'library',
            'install_path' => __DIR__ . '/../psr/http-client',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'psr/http-client-implementation' => array(
            'dev_requirement' => false,
            'provided' => array(
                0 => '1.0',
            ),
        ),
        'psr/http-factory' => array(
            'pretty_version' => '1.0.1',
            'version' => '1.0.1.0',
            'reference' => '12ac7fcd07e5b077433f5f2bee95b3a771bf61be',
            'type' => 'library',
            'install_path' => __DIR__ . '/../psr/http-factory',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'psr/http-factory-implementation' => array(
            'dev_requirement' => false,
            'provided' => array(
                0 => '1.0',
            ),
        ),
        'psr/http-message' => array(
            'pretty_version' => '1.0.1',
            'version' => '1.0.1.0',
            'reference' => 'f6561bf28d520154e4b0ec72be95418abe6d9363',
            'type' => 'library',
            'install_path' => __DIR__ . '/../psr/http-message',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'psr/http-message-implementation' => array(
            'dev_requirement' => false,
            'provided' => array(
                0 => '1.0',
            ),
        ),
        'psr/log' => array(
            'pretty_version' => '1.1.4',
            'version' => '1.1.4.0',
            'reference' => 'd49695b909c3b7628b6289db5479a1c204601f11',
            'type' => 'library',
            'install_path' => __DIR__ . '/../psr/log',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'psr/simple-cache' => array(
            'pretty_version' => '1.0.1',
            'version' => '1.0.1.0',
            'reference' => '408d5eafb83c57f6365a3ca330ff23aa4a5fa39b',
            'type' => 'library',
            'install_path' => __DIR__ . '/../psr/simple-cache',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'qcloud/cos-sdk-v5' => array(
            'pretty_version' => 'v2.5.0',
            'version' => '2.5.0.0',
            'reference' => '9aa2c598dc8cc8e7b7c0d85a0ee1998465eade72',
            'type' => 'library',
            'install_path' => __DIR__ . '/../qcloud/cos-sdk-v5',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'qiniu/php-sdk' => array(
            'pretty_version' => 'v7.4.1',
            'version' => '7.4.1.0',
            'reference' => '10c7ead8357743b4b987a335c14964fb07700d57',
            'type' => 'library',
            'install_path' => __DIR__ . '/../qiniu/php-sdk',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'ralouphie/getallheaders' => array(
            'pretty_version' => '3.0.3',
            'version' => '3.0.3.0',
            'reference' => '120b605dfeb996808c31b6477290a714d356e822',
            'type' => 'library',
            'install_path' => __DIR__ . '/../ralouphie/getallheaders',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'symfony/deprecation-contracts' => array(
            'pretty_version' => 'v2.5.0',
            'version' => '2.5.0.0',
            'reference' => '6f981ee24cf69ee7ce9736146d1c57c2780598a8',
            'type' => 'library',
            'install_path' => __DIR__ . '/../symfony/deprecation-contracts',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'symfony/finder' => array(
            'pretty_version' => 'v5.4.2',
            'version' => '5.4.2.0',
            'reference' => 'e77046c252be48c48a40816187ed527703c8f76c',
            'type' => 'library',
            'install_path' => __DIR__ . '/../symfony/finder',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'symfony/polyfill-mbstring' => array(
            'pretty_version' => 'v1.24.0',
            'version' => '1.24.0.0',
            'reference' => '0abb51d2f102e00a4eefcf46ba7fec406d245825',
            'type' => 'library',
            'install_path' => __DIR__ . '/../symfony/polyfill-mbstring',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'symfony/polyfill-php72' => array(
            'pretty_version' => 'v1.24.0',
            'version' => '1.24.0.0',
            'reference' => '9a142215a36a3888e30d0a9eeea9766764e96976',
            'type' => 'library',
            'install_path' => __DIR__ . '/../symfony/polyfill-php72',
            'aliases' => array(),
            'dev_requirement' => true,
        ),
        'symfony/polyfill-php80' => array(
            'pretty_version' => 'v1.24.0',
            'version' => '1.24.0.0',
            'reference' => '57b712b08eddb97c762a8caa32c84e037892d2e9',
            'type' => 'library',
            'install_path' => __DIR__ . '/../symfony/polyfill-php80',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'symfony/var-dumper' => array(
            'pretty_version' => 'v4.4.36',
            'version' => '4.4.36.0',
            'reference' => '02685c62fcbc4262235cc72a54fbd45ab719ce3c',
            'type' => 'library',
            'install_path' => __DIR__ . '/../symfony/var-dumper',
            'aliases' => array(),
            'dev_requirement' => true,
        ),
        'symfony/var-exporter' => array(
            'pretty_version' => 'v5.4.2',
            'version' => '5.4.2.0',
            'reference' => '2360c8525815b8535caac27cbc1994e2fa8644ba',
            'type' => 'library',
            'install_path' => __DIR__ . '/../symfony/var-exporter',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'topthink/framework' => array(
            'pretty_version' => 'v6.0.11',
            'version' => '6.0.11.0',
            'reference' => 'd9cadb6971ae92ff85ba5f2be77a40b0ad5718fb',
            'type' => 'library',
            'install_path' => __DIR__ . '/../topthink/framework',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'topthink/think' => array(
            'pretty_version' => '1.0.0+no-version-set',
            'version' => '1.0.0.0',
            'reference' => NULL,
            'type' => 'project',
            'install_path' => __DIR__ . '/../../',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'topthink/think-captcha' => array(
            'pretty_version' => 'v3.0.3',
            'version' => '3.0.3.0',
            'reference' => '1eef3717c1bcf4f5bbe2d1a1c704011d330a8b55',
            'type' => 'library',
            'install_path' => __DIR__ . '/../topthink/think-captcha',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'topthink/think-helper' => array(
            'pretty_version' => 'v3.1.6',
            'version' => '3.1.6.0',
            'reference' => '769acbe50a4274327162f9c68ec2e89a38eb2aff',
            'type' => 'library',
            'install_path' => __DIR__ . '/../topthink/think-helper',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'topthink/think-multi-app' => array(
            'pretty_version' => 'v1.0.14',
            'version' => '1.0.14.0',
            'reference' => 'ccaad7c2d33f42cb1cc2a78d6610aaec02cea4c3',
            'type' => 'library',
            'install_path' => __DIR__ . '/../topthink/think-multi-app',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'topthink/think-orm' => array(
            'pretty_version' => 'v2.0.50',
            'version' => '2.0.50.0',
            'reference' => '091ad5e023c15fcce4ceaea2f3814bdf71045cde',
            'type' => 'library',
            'install_path' => __DIR__ . '/../topthink/think-orm',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'topthink/think-template' => array(
            'pretty_version' => 'v2.0.8',
            'version' => '2.0.8.0',
            'reference' => 'abfc293f74f9ef5127b5c416310a01fe42e59368',
            'type' => 'library',
            'install_path' => __DIR__ . '/../topthink/think-template',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'topthink/think-trace' => array(
            'pretty_version' => 'v1.4',
            'version' => '1.4.0.0',
            'reference' => '9a9fa8f767b6c66c5a133ad21ca1bc96ad329444',
            'type' => 'library',
            'install_path' => __DIR__ . '/../topthink/think-trace',
            'aliases' => array(),
            'dev_requirement' => true,
        ),
        'topthink/think-view' => array(
            'pretty_version' => 'v1.0.14',
            'version' => '1.0.14.0',
            'reference' => 'edce0ae2c9551ab65f9e94a222604b0dead3576d',
            'type' => 'library',
            'install_path' => __DIR__ . '/../topthink/think-view',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
    ),
);
