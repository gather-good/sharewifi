<?php
namespace hema\device\engine;

use hema\Http;

/**
 * 河马云叫号器
 */
class Hmcalling extends Basics
{
    /**
    * 推送消息
    */
    public function push($data)
    {
        $params = $this->params();
        $params['id'] = $data['dev_id'];//设备ID
        $msg_data = [
            'type' => $data['mode'],//播报模式
            'data' => $data['row_no']
        ];
        $params['msg_data'] = hema_json($msg_data);
        $url = $this->config['api_url'] . '/api/device/push';
        $result = json_decode(Http::post($url,$params),true);
        if(!isset($result['code'])){
            return false;
        }
        if($result['code'] != 0){
            $this->error = $result['msg'];
            return false;
        }
        return $result;//返回数据格式：{"code":0,"msg":"成功"}
    }
    
    /**
    * 授权绑定设备
    */
    public function add($data)
    {
        $params = $this->params();
        $params['name'] = $data['dev_name'];//自定义名称(可填)
        $params['id'] = $data['dev_id'];//设备ID
        $params['key'] = $data['dev_key'];//设备密钥
        $url = $this->config['api_url'] . '/api/device/add';
        $result = json_decode(Http::post($url,$params),true);
        if(!isset($result['code'])){
            return false;
        }
        if($result['code'] != 0){
            $this->error = $result['msg'];
            return false;
        }
        return $result;
    }

    /**
    * 修改绑定设备
    */
    public function edit($data)
    {
        $params = $this->params();
        $params['name'] = $data['dev_name'];//自定义名称(可填)
        $params['id'] = $data['dev_id'];//设备ID
        $params['key'] = $data['dev_key'];//设备密钥
        $url = $this->config['api_url'] . '/api/device/edit';
        $result = json_decode(Http::post($url,$params),true);
         if(!isset($result['code'])){
            return false;
        }
        if($result['code'] != 0){
            $this->error = $result['msg'];
            return false;
        }
        return $result;//返回数据格式：{"code":0,"msg":"成功"}
    }
    
    /**
    * 删除授权绑定的设备
    */
    public function delete($dev_id)
    {
        $params = $this->params();
        $params['id'] = $dev_id;//设备ID
        $url = $this->config['api_url'] . '/api/device/delete';
        $result = json_decode(Http::post($url,$params),true);
        if(!isset($result['code'])){
            return false;
        }
        if($result['code'] != 0){
            $this->error = $result['msg'];
            return false;
        }
        return $result;//返回数据格式：{"code":0,"msg":"成功"}
    }
    
    /**
    * 获取设备状态接口 
    */
    public function status($dev_id)
    {
        $params = $this->params();
        $params['id'] = $dev_id;//编号
        $url = $this->config['api_url'] . '/api/device/status';
        $result = json_decode(Http::post($url,$params),true);
        if(!isset($result['code'])){
            return '未知';
        }
        if($result['code'] != 0){
            $this->error = $result['msg'];
            return false;
        }
        return $result['data']['text'];
        //返回格式 {'value':状态值,'text':状态}
    }

    /**
    * 公共参数
    */
    private function params(){
        $time = time();         //请求时间
        return $params = [
            'secret_id' => $this->config['app_key'],
            'secret_key' => $this->config['app_secret'],
            'timestamp' => $time,
            'type' => 'call',
            'sign' => $this->getSign($time)
        ];
    }
    
    /**
    * 生成签名
    */
    private function getSign($timestamp)
    {
        return md5($this->config['app_key'] . $timestamp . $this->config['app_secret']);
    }
}
