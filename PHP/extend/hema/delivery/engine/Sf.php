<?php
namespace hema\delivery\engine;

/**
 * 顺丰同城
*/
class Sf extends Basics
{
    /*
	物品类型product_type枚举值：
	1:快餐，2:送药，3:百货，4:脏衣服收，5:干净衣服派，6:生鲜，7:保单，8:高端饮品，9:现场勘验，10:快递
	12:文件，13:蛋糕，14:鲜花，15:电子数码，16:服装鞋帽，17:汽车配件，18:珠宝，20:披萨，21:中餐，22:水产
	27:专人直送，32:中端饮品，32:便利店，32:面包糕点，32:火锅，32:证照，32:其他
	*/
	
	/**
     * 预发布订单创建
	*/
	public function preOrder($data)
	{
	    $data['dev_id'] = $this->config['app_key'];//开发者ID
		$result = $this->post($data, "/open/api/external/precreateorder?sign=");
		if($result['error_code'] == 0){
			$price = (int)$result['result']['charge_price_list']['shop_pay_price'];//配送费单位分
			$price = round($price / 100,2); //四舍五入保留两位小数
			return [
				'delivery_time' => $result['result']['promise_delivery_time'] . '分钟',//预计送达时间
				'delivery_distance' => $result['result']['delivery_distance_meter'] . '米',//配送距离
				'delivery_price' => '￥' . $price . '元',//配送费用
				'time' => $result['result']['promise_delivery_time'],//预计送达时间
				'distance' => $result['result']['delivery_distance_meter'],//配送距离
				'price' => $price,//配送费用
				'sf' => $result
			];
		}
		$this->error = $result['error_msg'];
		return false;
	}

	/**
     * 创建订单
	*/
	public function addOrder($data)
	{
		$data['dev_id'] = $this->config['app_key'];//开发者ID
		$result =  $this->post($data, "/open/api/external/createorder?sign=");
		if($result['error_code'] == 0){
			return $result;
		}
		$this->error = $result['error_msg'];
		return false;
	}

	/**
     * 取消订单
	*/
	public function cancelOrder($order_no)
	{
		/*
		取消原因ID
		300	计划有变，暂时不需要寄件了
		302	填错订单信息，取消后重新提交
		303	骑士要求取消
		304	暂时无法提供待配送物品
		306	重复下单，取消此单
		309	骑士上门时间太长
		312	无人接单，换用其他平台寄件
		313	其他，请注明原因
		 */
		$post_data = [
			'dev_id' => $this->config['app_key'],//开发者ID
			'order_id' => $order_no,//顺丰订单ID
			'order_type' => 1,//1、顺丰订单号 2、商家订单号
			//'shop_id' => 0,//否	店铺ID	order_type=2时必传shop_id与shop_type
			//'shop_type' => 1,//否	店铺ID类型	1、顺丰店铺ID 2、接入方店铺ID
			//'cancel_code' => 313,//取消原因代码 不填时默认cancel_code=313,cancel_reason=商家发起取消
			//'cancel_reason' => '',//其他取消原因
			'push_time' => time(),//取消时间；秒级时间戳
		];
		$result =  $this->post($post_data, "/open/api/external/cancelorder?sign=");
		if($result['error_code'] == 0){
			return $result;
		}
		$this->error = $result['error_msg'];
		return false;
	}

    /**
     * 签名生成signature
     */
    private function _sign($data)
	{
		//$data = json_encode($data);
		$signChar = $data . '&'. $this->config['app_key'] . '&' . $this->config['app_secret'];
		$sign = base64_encode(MD5($signChar)); // 注：md5出来的结果是32位小写16进制字符串，$sign 的最终结果末尾包含等号=
		return $sign;
    }

    /**
     * 发送请求,POST
     * @param $url 指定URL完整路径地址
     * @param $data 请求的数据
     */
    private function post($data,$url)
	{
		$data = json_encode($data,JSON_UNESCAPED_UNICODE);
		$sign = $this->_sign($data);
        $url = $this->config['api_url'] . $url . $sign;
		// json
        $headers = array(
            'Content-Type: application/json',
        );
        $curl = curl_init($url);
		curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_HEADER, false);
        curl_setopt($curl, CURLOPT_POST, true);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
        curl_setopt($curl, CURLOPT_TIMEOUT, 3);
        curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);
        $resp = curl_exec($curl);
        //var_dump( curl_error($curl) );//如果在执行curl的过程中出现异常，可以打开此开关查看异常内容。
        $info = curl_getinfo($curl);
        curl_close($curl);
        if (isset($info['http_code']) && $info['http_code'] == 200) {
            return json_decode($resp, true);
        }
        return false;
    } 
	
}
