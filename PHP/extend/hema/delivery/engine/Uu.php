<?php
namespace hema\delivery\engine;

/**
 * UU跑腿
*/
class Uu extends Basics
{
	/**
     * 预发布订单 - 计算订单价格
	*/
	public function preOrder($data)
	{
		$result = $this->request_post('/v2_0/getorderprice.ashx',$data);
		if($result['return_code'] == 'ok'){
			return [
				'delivery_time' => '30分钟', //预计送达时间
				'delivery_distance' => $result['distance'] . '米', //配送距离
				'delivery_price' => '￥' . $result['need_paymoney'] . '元', //配送费用
				'time' => '', //预计送达时间
				'distance' => $result['distance'], //配送距离
				'price' => $result['need_paymoney'], //配送费用
				'uu' => $result
			];
		}
		$this->error = $result['return_msg'];
		return false;
	}
	
	/**
     * 发布订单
	*/
	public function addOrder($data)
	{
		$result = $this->request_post('/v2_0/addorder.ashx',$data);
		if($result['return_code'] == 'ok'){
			return $result;
		}
		$this->error = $result['return_msg'];
		return false;
	}

	/**
     * 取消订单
	*/
	public function cancelOrder($order_no)
	{
		$post_data = [
			//'origin_id' => '',//第三方对接平台订单id，order_code和origin_id必须二选其一
			'order_code' => $order_no,//UU跑腿订单编号，order_code和origin_id必须二选其一
			'reason' => '商家取消',//取消原因
		];
		$result = $this->request_post('/v2_0/cancelorder.ashx',$post_data);
		if($result['return_code'] == 'ok'){
			return $result;
		}
		$this->error = $result['return_msg'];
		return false;
	}	

	/**
	 * 发起http post请求
	 */
	private function request_post($url = '', $post_data = array()) 
	{
		$post_data['appid'] = $this->config['app_id'];			//应用ID
		$post_data['openid'] = $this->config['open_id'];		//商户编号
        $post_data['timestamp'] = time();			//时间戳
        $post_data['nonce_str'] = $this->guid();		//随机字符串
		$post_data['sign'] = $this->sign($post_data);//签名
		$url = $this->config['api_url'] . $url;			//拼接API接口地址
		
		$arr = [];
		foreach ($post_data as $key => $value) {
		  $arr[] = $key.'='.$value;
		}
		$curlPost = implode('&', $arr);
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL,$url);
		curl_setopt($ch, CURLOPT_HEADER, 0);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $curlPost);
		$data = curl_exec($ch);
		curl_close($ch);
		return json_decode($data,true);
	}

	// 生成guid
	private function guid()
	{
		if (function_exists('com_create_guid')){
			$uuid = com_create_guid();
		}else{
			mt_srand((double)microtime()*10000);//optional for php 4.2.0 and up.
			$charid = strtoupper(md5(uniqid(rand(), true)));
			$hyphen = chr(45);// "-"
			$uuid = substr($charid, 0, 8).$hyphen
					.substr($charid, 8, 4).$hyphen
					.substr($charid,12, 4).$hyphen
					.substr($charid,16, 4).$hyphen
					.substr($charid,20,12);  
		}
		$uuid = str_replace('-', '', $uuid);
		return strtolower($uuid);
	}

	// 生成签名
	private function sign($data) 
	{
		ksort($data);//从小到大排序（字典序）
		$string = http_build_query($data);//构造URL字符串
		$string = urldecode($string);
		$string .= '&key='.$this->config['app_key'];
		$string = strtoupper($string);//字母转换为大写
		$string = md5($string);//MD5(32位)加密
		$sign = strtoupper($string);//字母转换为大写
		return $sign;
	}

	
	/**
     * 获取订单详情
	*/
	public function getorderdetail($order_id)
	{
		$post_data = [
			//'order_code' => $order_id,//UU跑腿订单编号，order_code和origin_id必须二选其一，如果都传，则只根据order_code返回
			'origin_id' => $order_id //第三方对接平台订单id，order_code和origin_id必须二选其一，如果都传，则只根据order_code返回
		];
		return $this->request_post('getorderdetail.ashx',$post_data);
	}
	
	/**
     * 获取已开通的城市列表
	*/
	public function getcitylist()
	{
		return $this->request_post('getcitylist.ashx');
	}
	
	/**
     * 获取余额详情
	*/
	public function getbalancedetail()
	{
		return $this->request_post('getbalancedetail.ashx');
	}
	
}
