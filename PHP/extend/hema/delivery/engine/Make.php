<?php
namespace hema\delivery\engine;

use think\facade\Cache;
use hema\Http;

/**
 * 码科配送
*/
class Make extends Basics
{
	/**
     * 预发布订单创建
	*/
	public function preOrder($data)
	{
	    $data['token'] = $this->_token(); //令牌
		$url = $this->config['api_url'] . '/addons/make_speed/core/public/index.php/apis/v2/get_delivery_price';
        $result = json_decode(Http::get($url, $data),true);
		if($result['error_code'] == 0){
			$distance = $result['data']['distance'] * 1000;//公里转换为米
			return [
				'delivery_time' => '30分钟',//预计送达时间
				'delivery_distance' => $distance . '米',//配送距离
				'delivery_price' => '￥' . $result['data']['total_price'] . '元', //配送费用
				'time' => '',//预计送达时间
				'distance' => $distance,//配送距离
				'price' => $result['data']['total_price'], //配送费用
				'make' => $result
			];
		}
		$this->error = $result['msg'];
		return false;
	}

	/**
     * 创建订单
	*/
	public function addOrder($data)
	{
	    $data['token'] = $this->_token(); //令牌
		$result =  $this->post($data, "/addons/make_speed/core/public/index.php/apis/v2/create_order");
		if($result['error_code'] == 0){
			return $result;
		}
		$this->error = $result['msg'];
		return false;
	}

	/**
     * 取消订单
	*/
	public function cancelOrder($order_no)
	{
		$post_data = [
			'token' => $this->_token(),//令牌
			'order_num' => $order_no,//订单号(配送平台)
		];
		$result =  $this->post($post_data, "/addons/make_speed/core/public/index.php/apis/v2/cancel_order");
		if($result['error_code'] == 0){
			return $result;
		}
		$this->error = $result['msg'];
		return false;
	}

    /**
     * 生成token
     */
    private function _token()
	{
		if($token = Cache::get('make_token')){
			return $token;
		}
		$post_data = [
			'token' => $this->config['token'],
			'appid' => $this->config['appid']
		];
		$result =  $this->post($post_data, "/addons/make_speed/core/public/index.php/apis/v2/get_token");
		if(isset($result['token'])){
			// 记录缓存, 70小时，不到3天（token有效期三天）
        	Cache::set('make_token', $result['token'], 3600 * 70);
			return $result['token'];
		}
		return '';
    }

    /**
     * 发送请求,POST
     * @param $url 指定URL完整路径地址
     * @param $data 请求的数据
     */
    private function post($data,$url)
	{
        $url = $this->config['api_url'] . $url;
		// json
        $headers = array(
            'Content-Type: multipart/form-data',
        );
        $curl = curl_init($url);
		curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_HEADER, false);
        curl_setopt($curl, CURLOPT_POST, true);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
        curl_setopt($curl, CURLOPT_TIMEOUT, 3);
        curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);
        $resp = curl_exec($curl);
        //var_dump( curl_error($curl) );//如果在执行curl的过程中出现异常，可以打开此开关查看异常内容。
        $info = curl_getinfo($curl);
        curl_close($curl);
        if (isset($info['http_code']) && $info['http_code'] == 200) {
            return json_decode($resp, true);
        }
        return false;
    } 
	
}
