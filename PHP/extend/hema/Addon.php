<?php
namespace hema;

use think\facade\Config;
use think\facade\Cache;
use hema\Http;

class Addon
{
    private $api_url = '';
    private $token = '';
    private $error;

    /**
     * 构造函数
     */
    public function __construct()
    {
        $this->api_url = Config::get('app.hemaphp.api_url');
        $user = Cache::get('hemaphp',[]);
        isset($user['token']) && $this->token = $user['token'];
    }


    /**
     * 获取插件列表
     */
    public function getAddonList($type = 'all', $page = 1)
    {
        $url = $this->api_url . '/api/addon/lists';
        $queryarr['page'] = $page;
        $type != 'all' && $queryarr['type'] = $type;
        $result = json_decode(Http::post($url, $queryarr),true);
        $addons = get_addons_list();
        for($n=0;$n<sizeof($result['data']['list']['data']);$n++){
            if(isset($addons[$result['data']['list']['data'][$n]['name']])){
                $result['data']['list']['data'][$n]['addon'] = $addons[$result['data']['list']['data'][$n]['name']];
            }
        }
        return $result['data'];
    }
    
    /**
     * 获取插件详情
     */
    public function getAddonDetail($name)
    {
        $url = $this->api_url . '/api/addon/detail';
        $queryarr['name'] = $name;
        return $this->result(json_decode(Http::post($url, $queryarr),true)); 
    }
    
    /**
     * 验证是否登录
     */
    public function checkLogin()
    {
        if($this->token){
            $url = $this->api_url . '/api/user/checkAddonLogin';
            $queryarr['token'] = $this->token;
            return $this->result(json_decode(Http::post($url, $queryarr),true)); 
        }
        $this->error = '未登录';
        return false;
        
    }
    
    /**
     * 验证是否登录
     */
    public function logout()
    {
        $url = $this->api_url . '/api/user/addonLogout';
        $queryarr['token'] = $this->token;
        $result = json_decode(Http::post($url, $queryarr),true); 
        if($result['code']!=0){
			$this->error = $result['msg'];
			return false;
		}
        Cache::delete('hemaphp');
		return true;
        
    }
    
    /**
     * 用户登录
     */
    public function login($queryarr)
    {
        $url = $this->api_url . '/api/user/addonLogin';
        $result = json_decode(Http::post($url, $queryarr),true);
        if($result['code']!=0){
			$this->error = $result['msg'];
			return false;
		}
		Cache::set('hemaphp',$result['data']);
		return $result['data'];
    }
    
    /**
     * 验证是否登录
     */
    public function news()
    {
        $url = $this->api_url . '/api/hema/version';
        $queryarr['domain'] = domain();
        if($result = json_decode(Http::post($url, $queryarr),true)){
            if(isset($result['code']) AND $result['code'] !=0 ){
    			$this->error = $result['msg'];
    			return false;
    		}
    		$version = config('app.hemaphp.version');
    		if(isset($result['data']['version']) AND $result['data']['version'] != $version){
    			return true;
    		}
        }
		return false;
        
    }
    
    /**
     * 请求数据验证
     **/
    private function result($result)
    {
        if($result['code']!=0){
			$this->error = $result['msg'];
			return false;
		}
		return $result['data'];
    }
    
    public function getError()
    {
        return $this->error;
    }

}
