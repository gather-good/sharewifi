<?php
namespace app\applet\model;

use app\common\model\WechatBatchSend as WechatBatchSendModel;

/**
 * 微信群发消息模型
 */
class WechatBatchSend extends WechatBatchSendModel
{
    /**
     * 更新状态
     */
    public function backEdit(array $data)
    {
		if($data['Status'] == 'send success'){
				$status = 30;
		}elseif($data['Status'] == 'send fail'){
				$status = 40;
		}else{
			switch($data['Status'])
			{
				case 'err(21000)':
					$status = 21000;
					break;  
				case 'err(30001)':
					$status = 30001;
					break;
				case 'err(30002)':
					$status = 30002;
					break;
				case 'err(30003)':
					$status = 30003;
					break;
				case 'err(40001)':
					$status = 40001;
					break;
				case 'err(40002)':
					$status = 40002;
					break;
				case 'err(22000)':
					$status = 22000;
					break;
				case 'err(20013)':
					$status = 20013;
					break;
				case 'err(20008)':
					$status = 20008;
					break;
				case 'err(20006)':
					$status = 20006;
					break;
				case 'err(20002)':
					$status = 20002;
					break;
				case 'err(20004)':
					$status = 20004;
					break;
				case 'err(20001)':
					$status = 20001;
					break;
				case 'err(10001)':
					$status = 10001;
					break;
				default:
					$status = 0;
			}
		}
        return $this->save([
			'status' => $status,
			'send_count' => $data['SentCount']
		]) !== false;
    }
}
