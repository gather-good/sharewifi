<?php
namespace app\applet\controller\wxapp;

use app\applet\controller\Controller;
use app\applet\model\TemplateCode as TemplateCodeModel;
use hema\wechat\Driver;
use think\facade\View;

/**
 * 小程序版本管理
 */
class Release extends Controller
{
    /**
     * 版本管理
     */
    public function index()
    {
        $wxapp = [];
        if($this->applet['status']['value'] == 1){
            $wx = new Driver;
            //查询小程序版本信息
            if(!$result = $wx->getVersionInfo($this->applet_id)){
            	return $this->renderError($wx->getError());
            }
            isset($result['exp_info']) && $wxapp['exp_info'] = $result['exp_info'];
            isset($result['release_info']) && $wxapp['release_info'] = $result['release_info'];
            //查询小程序服务状态
            if(!$result = $wx->getVisitStatus($this->applet_id)){
            	return $this->renderError($wx->getError());
            }
            isset($result['status']) && $wxapp['status'] = $result['status'];
            //获取平台推送的最新模板代码
            if($result = TemplateCodeModel::getNew($this->app_type)){
                $wxapp['code_new'] = $result;
            }
            
            //如果已有线上版本
            if(isset($wxapp['release_info']['release_version'])){
            	//获取平台推送的模板代码详情
            	if($result=TemplateCodeModel::getCode($this->app_type,$wxapp['release_info']['release_version'])){
            	    $wxapp['code_release'] = $result;
            	}
            }
            //如果已有开发版本
            if(isset($wxapp['exp_info']['exp_version'])){
            	//获取平台推送的模板代码详情
            	if($result=TemplateCodeModel::getCode($this->app_type,$wxapp['exp_info']['exp_version'])){
            	    $wxapp['code_exp'] = $result;
            	}
            }
            //查询最新一次审核单状态
            if($result = $wx->getLatestAuditStatus($this->applet_id)){
            	if(isset($wxapp['release_info']['release_version'])){
            		//如果有上线版本，且不等于审核中的版本
            		if(isset($result['user_version']) and $result['user_version'] != $wxapp['release_info']['release_version']){
            			$wxapp['audit_status'] = $result;
            		}
            	}else{
            		//如果没有上线版本，且有审核中的版本
            		if(isset($result['user_version'])){
            			$wxapp['audit_status'] = $result;
            		}
            	}
            }
        }
        return View::fetch('index', compact('wxapp'));
    }
    
    /**
     * 切换小程序服务状态
     */
    public function status($id)
    {
    	if($id == 1){
    	    $action = 'close';
    	}else{
    	    $action = 'open';
    	}
    	$wx = new Driver;
    	if(!$wx->setVisitStatus($this->applet_id,$action)){
			return $this->renderError($wx->getError());
		}
		return $this->renderSuccess('操作成功', url('wxapp.release/index'));
    }
    /**
     * 发布已通过审核的小程序
     */
    public function release()
    {
    	$wx = new Driver;
    	if(!$wx->release($this->applet_id)){
			return $this->renderError($wx->getError());
		}
		return $this->renderSuccess('操作成功', url('wxapp.release/index'));
    }
    /**
     * 版本回退
     */
    public function fallback()
    {
        $wx = new Driver;
        if ($this->request->isGet()) {
            //获取可回退的小程序版本
            if(!$result = $wx->getHistoryVersion($this->applet_id)){
                return $this->renderError($wx->getError());
            }
            $list = $result['version_list'];
            for($n=0;$n<sizeof($list);$n++){
                $list[$n]['commit_time'] = date('Y-m-d H:i:s',$list[$n]['commit_time']);
            }
            return $this->renderSuccess('', '', compact('list'));
        }
        $data = $this->postData('data');
        // 回退到指定版本
        if ($wx->revertCodeRelease($this->applet_id,$data['app_version'])) {
            return $this->renderSuccess('操作成功', url('wxapp.release/index'));
        }
        $error = $wx->getError() ?: '操作失败';
        return $this->renderError($error);
    }

    /**
     * 撤回代码审核
     */
    public function undoAudit()
    {
    	$wx = new Driver;
    	if(!$wx->undoAudit($this->applet_id)){
			return $this->renderError($wx->getError());
		}
		return $this->renderSuccess('操作成功', url('wxapp.release/index'));
    }
    /**
     * 提交代码审核
     */
    public function submitAudit()
    {
        if ($this->request->isPost()) {
            $data = $this->postData('data');
            $wx = new Driver;
            if ($wx->submitAudit($this->applet_id,$data['speedup'])) {
                return $this->renderSuccess('提交成功', url('wxapp.release/index'));
            }
            $error = $wx->getError() ?: '提交失败';
            return $this->renderError($error);
        }
        return $this->renderError('非法请求');
    }
    
    /**
     * 获取体验版二维码
     */
    public function trialQrcode()
    {
    	$wx = new Driver;
    	if($path = $wx->getTrialQRCode($this->applet_id)){
			return $this->renderSuccess('', '', compact('path'));
		}
		return $this->renderError($wx->getError());
    }
    /**
     * 上传代码并生成体验版
     */
    public function commit($id)
    {
    	$detail = TemplateCodeModel::detail($id);
    	$wx = new Driver;
    	if(!$wx->commit($this->applet,$detail)){
			return $this->renderError($wx->getError());
		}
		return $this->renderSuccess('操作成功', url('wxapp.release/index'));
    }
}
