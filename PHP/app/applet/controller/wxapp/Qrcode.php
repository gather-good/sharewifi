<?php
namespace app\applet\controller\wxapp;

use app\applet\controller\Controller;
use hema\wechat\Driver;
use think\facade\View;

/**
 * 二维码控制器
 */
class Qrcode extends Controller
{
     /**
     * 列表
     */
    public function index()
    {
        $wx = new Driver;
        $list = $wx->getJumpQRCode($this->applet_id);
		return View::fetch('index', compact('list'));
    }

    /**
     * 添加
     */
    public function add()
    {
        if ($this->request->isPost()) {
            $data = $this->postData('data');
            $data['is_edit'] = 0;
            $data['prefix'] = base_url() . $data['prefix'];
            $wx = new Driver;
            if ($wx->addJumpQRCode($this->applet_id,$data)) {
                return $this->renderSuccess('添加成功', url('wxapp.qrcode/index'));
            }
            $error = $wx->getError() ?: '添加失败';
            return $this->renderError($error);
        }
        return redirect(url('wxapp.qrcode/index'));
    }

    /**
     * 编辑
     */
    public function edit()
    {
        if ($this->request->isPost()) {
            $data = $this->postData('data');
            $data['is_edit'] = 1;
            $data['prefix'] = base_url() . $data['prefix'];
            $wx = new Driver;
            if ($wx->addJumpQRCode($this->applet_id,$data)) {
                return $this->renderSuccess('修改成功', url('wxapp.qrcode/index'));
            }
            $error = $wx->getError() ?: '修改失败';
            return $this->renderError($error);
        }
        return redirect(url('wxapp.qrcode/index'));
    }
    
    /**
     * 发布规则
     */
    public function status($id)
    {
        $wx = new Driver;
        if ($wx->publishJumpQRCode($this->applet_id,$id)) {
            return $this->renderSuccess('发布成功', url('wxapp.qrcode/index'));
        }
        $error = $wx->getError() ?: '发布失败';
        return $this->renderError($error);
    }
    
    /**
     * 删除
     */
    public function delete($id)
    {
        $wx = new Driver;
        if ($wx->deleteJumpQRCode($this->applet_id,$id)) {
            return $this->renderSuccess('删除成功', url('wxapp.qrcode/index'));
        }
        $error = $wx->getError() ?: '删除失败';
        return $this->renderError($error);
    }

}
