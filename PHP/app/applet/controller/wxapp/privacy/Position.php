<?php
namespace app\applet\controller\wxapp\privacy;

use app\applet\controller\Controller;
use hema\wechat\Driver;
use think\facade\View;

/**
 * 小程序地理位置接口管理
 */
class Position extends Controller
{
    /**
     * 列表
     */
    public function index()
    {
		$wx = new Driver;
        $list = $wx->getPrivacyInterface($this->applet_id);
		return View::fetch('index', compact('list'));	
    }

    /**
     * 添加
     */
    public function add()
    {
    	if ($this->request->isPost()) {
            $wx = new Driver;
            $data = $this->postData('data');
            $data['url_list'] = [base_url()];
            if ($wx->applyPrivacyInterface($this->applet_id,$data)) {
                return $this->renderSuccess('操作成功', url('wxapp.privacy.position/index'));
            }
            $error = $wx->getError() ?: '操作失败';
            return $this->renderError($error);
        }
        return redirect(url('wxapp.privacy.position/index'));
    }
}
