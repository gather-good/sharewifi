<?php
namespace app\applet\controller\wxapp;

use app\applet\controller\Controller;
use app\applet\model\Apply as ApplyModel;
use app\applet\model\UserDetail as UserDetailModel;
use app\applet\model\Setting;
use think\facade\View;

/**
 * 用户注册申请
 */
class Register extends Controller
{
    /**
     * 构造方法
     */
    public function initialize()
    {
        parent::initialize();
        View::assign(['ability' => Setting::getItem('ability',0)]);
    }
    /**
     * 注册小程序
     */
    public function wxapp()
    {	
    	$model = ApplyModel::getApply([
    		'apply_mode' => 10,
    		'applet_id' => $this->applet_id,
    		'user_id' => $this->user['user']['user_id']
    	]);
		if(!$this->request->isAjax()) {
			if(!$model){
				$model['details'] = UserDetailModel::getUserDetail($this->user['user']['user_id']);
			}
			return View::fetch('wxapp', compact('model'));
        }
        //提交动作
		$data = $this->postData('data');
		if(!isset($data['details']['license_copy'])){
			if(!$file = request()->file('license_copy')){
               return $this->renderError('请上传营业执照'); 
			}
			$file_path = \think\facade\Filesystem::disk('public')->putFile('user',$file);
			$data['details']['license_copy'] = $file_path;
		}
		if(!isset($data['details']['id_card_copy'])){
			if(!$file = request()->file('id_card_copy')){
               return $this->renderError('请上传身份证（正面）'); 
			}
			$file_path = \think\facade\Filesystem::disk('public')->putFile('user',$file);
			$data['details']['id_card_copy'] = $file_path;
		}
		if(!isset($data['details']['id_card_national'])){
			if(!$file = request()->file('id_card_national')){
               return $this->renderError('请上传身份证（反面）'); 
			}
			$file_path = \think\facade\Filesystem::disk('public')->putFile('user',$file);
			$data['details']['id_card_national'] = $file_path;
		}
		if(!$model){
			$model = new ApplyModel;
			$data['business_code'] = order_no();//业务申请编号
			$data['user_id'] = $this->user['user']['user_id'];
			$data['applet_id'] = $this->applet_id;
		}
        if ($model->action($data)) {
			return $this->renderSuccess('提交成功，等待审核', url('wxapp/index'));
        }
        $error = $model->getError() ?: '提交失败';
        return $this->renderError($error);      
    }

    /**
     * 注册支付商户号
     */
    public function wxpay()
    {	
    	$model = ApplyModel::getApply([
    		'apply_mode' => 30,
    		'applet_id' => $this->applet_id,
    		'user_id' => $this->user['user']['user_id']
    	]);
        if(!$this->request->isAjax()) {
			if(!$model){
				$model['details'] = UserDetailModel::getUserDetail($this->user['user']['user_id']);
			}
			return View::fetch('wxpay', compact('model'));
        }
        //提交动作
		$data = $this->postData('data');
		if(!isset($data['details']['license_copy'])){
			if(!$file = request()->file('license_copy')){
               return $this->renderError('请上传营业执照'); 
			}
			$file_path = \think\facade\Filesystem::disk('public')->putFile('user',$file);
			$data['details']['license_copy'] = $file_path;
		}
		if(!isset($data['details']['id_card_copy'])){
			if(!$file = request()->file('id_card_copy')){
               return $this->renderError('请上传身份证（正面）'); 
			}
			$file_path = \think\facade\Filesystem::disk('public')->putFile('user',$file);
			$data['details']['id_card_copy'] = $file_path;
		}
		if(!isset($data['details']['id_card_national'])){
			if(!$file = request()->file('id_card_national')){
               return $this->renderError('请上传身份证（反面）'); 
			}
			$file_path = \think\facade\Filesystem::disk('public')->putFile('user',$file);
			$data['details']['id_card_national'] = $file_path;
		}
		if(!isset($data['details']['qualifications'])){
			if(!$file = request()->file('qualifications')){
               return $this->renderError('请上传食品经营许可证'); 
			}
			$file_path = \think\facade\Filesystem::disk('public')->putFile('user',$file);
			$data['details']['qualifications'] = $file_path;
		}
		
		if(!isset($data['details']['store_entrance_pic'])){
			if(!$file = request()->file('store_entrance_pic')){
               return $this->renderError('请上传门头照片'); 
			}
			$file_path = \think\facade\Filesystem::disk('public')->putFile('user',$file);
			$data['details']['store_entrance_pic'] = $file_path;
		}
		if(!isset($data['details']['indoor_pic'])){
			if(!$file = request()->file('indoor_pic')){
               return $this->renderError('请上传店内照片1111'); 
			}
			$file_path = \think\facade\Filesystem::disk('public')->putFile('user',$file);
			$data['details']['indoor_pic'] = $file_path;
		}
		$data['app_id'] = $this->user['applet']['app_id'];
		if(!$model){
			$model = new ApplyModel;
			$data['business_code'] = order_no();//业务申请编号
			$data['user_id'] = $this->user['user']['user_id'];
			$data['applet_id'] = $this->applet_id;
		}
		if ($model->action($data)) {
			return $this->renderSuccess('提交成功，等待审核', url('payment/wxpay'));
        }
        $error = $model->getError() ?: '提交失败';
        return $this->renderError($error);   
    }

}
