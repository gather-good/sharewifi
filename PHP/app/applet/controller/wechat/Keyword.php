<?php
namespace app\applet\controller\wechat;

use app\applet\controller\Controller;
use app\applet\model\Keyword as KeywordModel;
use app\applet\model\Wechat as WechatModel;
use think\facade\View;

/**
 * 关键字回复控制器
 */
class Keyword extends Controller
{
	/**
     * 首页
     */
    public function index()
    {
        $wechat = WechatModel::detail();
        $model = new KeywordModel;
        $list = $model->getList();
        return View::fetch('index', compact('list','wechat'));
    }
	
	/**
     * 添加
     */
    public function add()
    {
        if ($this->request->isPost()) {
            if(!$wechat = WechatModel::detail()){
                return $this->renderError('还未绑定公众号');
            }
            if($wechat['status']['value'] == 0){
                return $this->renderError('还未绑定公众号');
            }
            $model = new KeywordModel;
            // 新增记录
            if ($model->add($this->postData('data'))) {
                return $this->renderSuccess('添加成功', url('wechat.keyword/index'));
            }
            $error = $model->getError() ?: '添加失败';
            return $this->renderError($error);
        }
        return redirect(url('wechat.keyword/index')); 
    }

    /**
     * 删除
     */
    public function delete($id)
    {
        $model = KeywordModel::get($id);
        if (!$model->remove()) {
            return $this->renderError('删除失败');
        }
        return $this->renderSuccess('删除成功');
    }

    /**
     * 编辑
     */
    public function edit($id)
    {
        $model = KeywordModel::get($id);
        if ($this->request->isGet()) {
            if($model){
               return $this->renderSuccess('', '', compact('model')); 
            }
            return $this->renderError('获取失败');
        }
        // 更新记录
		if(!$wechat = WechatModel::detail()){
            return $this->renderError('还未绑定公众号');
        }
        if($wechat['status']['value'] == 0){
            return $this->renderError('还未绑定公众号');
        }
        // 更新记录
        if ($model->edit($this->postData('data'))) {
            return $this->renderSuccess('更新成功', url('wechat.keyword/index'));
        }
        $error = $model->getError() ?: '更新失败';
        return $this->renderError($error);
    }
	
	/**
     * 状态编辑
     */
    public function status($id)
    {
        $model = KeywordModel::get($id);
        // 更新记录
        if ($model->status()) {
            return $this->renderSuccess('更新成功', url('wechat.keyword/index'));
        }
        $error = $model->getError() ?: '更新失败';
        return $this->renderError($error);
    }

}
