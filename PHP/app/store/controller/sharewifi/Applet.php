<?php
namespace app\store\controller\sharewifi;

use app\store\controller\sharewifi\Controller;
use app\store\model\sharewifi\Setting as SettingModel;
use think\facade\View;

/**
 * 小程序设置
 */
class Applet extends Controller
{	
    /**
     * 公告设置
     */
    public function notice()
    {
        return $this->updateEvent('notice');
    }
    
    /**
     * 轮播图片
     */
    public function swiper()
    {
        return $this->updateEvent('swiper');
    }
    
    /**
     * 分享设置
     */
    public function share()
    {
        return $this->updateEvent('share');
    }
    
    /**
     * 更新设置事件
     */
    private function updateEvent(string $key)
    {
        if (!$this->request->isAjax()) {
			$model = SettingModel::getItem($key);
            return View::fetch($key, compact('model'));
        }
        $model = new SettingModel;
        if($model->edit($key, $this->postData('data'))) {
            return $this->renderSuccess('更新成功');
        }
        $error = $model->getError() ?: '更新失败';
        return $this->renderError($error);
    }
}
