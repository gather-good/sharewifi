<?php
namespace app\store\controller\sharewifi;

use app\store\controller\sharewifi\Controller;
use think\facade\View;
use think\facade\Session;
use think\captcha\facade\Captcha;
use app\store\model\sharewifi\Config as ConfigModel;


/**
 * 登录管理中心
 */
class Passport extends Controller
{
    /**
     * 生成验证码
     **/
    public function captcha()
    {
        return Captcha::create();
    }

    /**
     * 用户登录
     */
    public function login()
    {
        if (!$this->request->isAjax()) {
            // 验证登录状态
            if (isset($this->user) AND (int)$this->user['is_login'] === 1) {
                return redirect(url($this->app_type . '.index/index'));
            }
            View::layout(false);
            return View::fetch();
        }
        $model = new ConfigModel;
        if ($model->login($this->postData('data'))) {
            return $this->renderSuccess('登录成功', url($this->app_type . '.index/index'));
        }
        $error = $model->getError() ?: '登录失败';
        return $this->renderError($error);
    }

}