<?php
namespace app\store\controller\sharewifi\user;

use app\store\controller\sharewifi\Controller;
use app\store\model\sharewifi\Apply as ApplyModel;
use think\facade\View;

/**
 * 申请记录管理
 */
class Apply extends Controller
{
    /**
     * 列表
     */
    public function index()
    {
        $model = new ApplyModel;
        $list = $model->getList();
        return View::fetch('index', compact('list'));
    }

    /**
     * 编辑
     */
    public function edit($id)
    {
        $model = ApplyModel::get($id);
        if ($this->request->isGet()) {
            if($model){
               return $this->renderSuccess('', '', compact('model')); 
            }
            return $this->renderError('获取失败');
        }
        // 更新记录
        if ($model->edit($this->postData('data'))) {
            return $this->renderSuccess('操作成功', url('sharewifi.user.apply/index'));
        }
        $error = $model->getError() ?: '操作失败';
        return $this->renderError($error);
    }

    /**
     * 删除
     */
    public function delete($id)
    {
        $model = ApplyModel::get($id);
        if ($model->remove()) {
            return $this->renderSuccess('删除成功');
        }
        $error = $model->getError() ?: '删除失败';
        return $this->renderError($error);
        
    }

}
