<?php
namespace app\store\controller\sharewifi\user;

use app\store\controller\sharewifi\Controller;
use app\store\model\sharewifi\Wallet as WalletModel;
use think\facade\View;

/**
 * 钱包请记录模型
 */
class Wallet extends Controller
{
    /**
     * 提现记录
     */
    public function transfer()
    {
        $model = new WalletModel;
        $list = $model->getList(10);
        return View::fetch('transfer', compact('list'));
    }
    
    /**
     * 分红记录
     */
    public function share()
    {
        $model = new WalletModel;
        $list = $model->getList(20);
        return View::fetch('share', compact('list'));
    }

    /**
     * 编辑
     */
    public function edit($id)
    {
        $model = WalletModel::get($id);
        if ($this->request->isGet()) {
            if($model){
               return $this->renderSuccess('', '', compact('model')); 
            }
            return $this->renderError('获取失败');
        }
        // 更新记录
        if ($model->edit($this->postData('data'))) {
            return $this->renderSuccess('操作成功', url('sharewifi.user.wallet/transfer'));
        }
        $error = $model->getError() ?: '操作失败';
        return $this->renderError($error);
    }

    /**
     * 删除
     */
    public function delete($id)
    {
        $model = WalletModel::get($id);
        if ($model->remove()) {
            return $this->renderSuccess('删除成功');
        }
        $error = $model->getError() ?: '删除失败';
        return $this->renderError($error);
        
    }

}
