<?php
namespace app\store\controller\sharewifi\user;

use app\store\controller\sharewifi\Controller;
use app\store\model\sharewifi\Connect as ConnectModel;
use think\facade\View;

/**
 * WiFi连接记录
 */
class Connect extends Controller
{
    /**
     * 列表
     */
    public function index()
    {
        $model = new ConnectModel;
        $list = $model->getList();
        return View::fetch('index', compact('list'));
    }

}
