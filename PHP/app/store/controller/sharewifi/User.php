<?php
namespace app\store\controller\sharewifi;

use app\store\controller\sharewifi\Controller;
use app\store\model\sharewifi\User as UserModel;
use think\facade\View;

/**
 * 用户管理
 */
class User extends Controller
{
	
	/**
     * 列表
     */
    public function index($search = '')
    {
        $model = new UserModel;
        $list = $model->getList(0,$search);
        return View::fetch('index', compact('list','search'));
    }
}
