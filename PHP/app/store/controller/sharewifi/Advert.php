<?php
namespace app\store\controller\sharewifi;

use app\store\controller\sharewifi\Controller;
use app\store\model\sharewifi\Setting as SettingModel;
use think\facade\View;

/**
 * 广告管理
 */
class Advert extends Controller
{
    /**
     * 广告设置
     */
    public function advert()
    {
        if (!$this->request->isAjax()) {
			$model = SettingModel::getItem('advert');
            return View::fetch('advert', compact('model'));
        }
        $model = new SettingModel;
        if($model->edit('advert', $this->postData('data'))) {
            return $this->renderSuccess('更新成功');
        }
        $error = $model->getError() ?: '添加失败';
        return $this->renderError($error);
    }
    
    /**
     * 广告统计
     */
    public function census()
    {
        echo '敬请期待...';
    }
}
