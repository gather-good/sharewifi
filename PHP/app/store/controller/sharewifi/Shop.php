<?php
namespace app\store\controller\sharewifi;

use app\store\controller\sharewifi\Controller;
use app\store\model\sharewifi\Category as CategoryModel;
use app\store\model\sharewifi\Shop as ShopModel;
use think\facade\View;

/**
 * 商家控制器
 */
class Shop extends Controller
{
	/**
     * 已分类商家
     */
    public function cat_ok()
    {
        return $this->getList('已分类商家',1);
    }
    
    /**
     * 待分类商家
     */
    public function cat_no()
    {
        return $this->getList('待分类商家',0);
    }
    
    /**
     * 获取列表
     */
    private function getList(string $title, $is_category)
    { 
        $model = new ShopModel;
        $list = $model->getList($is_category);
        return View::fetch('index', compact('title','list'));
    }
    
    /**
     * 删除
     */
    public function delete($id)
    {
        $model = ShopModel::get($id);
        if (!$model->remove()) {
            $error = $model->getError() ?: '删除失败';
            return $this->renderError($error);
        }
        return $this->renderSuccess('删除成功');
    }

    /**
     * 编辑
     */
    public function edit($id)
    {
        $model = ShopModel::get($id);
        if ($this->request->isGet()) {
            $category = new CategoryModel;
            $category = $category->getList();
            if($model){
               return $this->renderSuccess('', '', compact('model','category')); 
            }
            return $this->renderError('获取失败');
        }
        // 更新记录
        if ($model->edit($this->postData('data'))) {
            return $this->renderSuccess('操作成功', url('sharewifi.shop/cat_ok'));
        }
        $error = $model->getError() ?: '操作失败';
        return $this->renderError($error);
    }
    
    /**
     * 状态
     */
    public function status($id)
    {
        $model = ShopModel::get($id);
        if (!$model->status()) {
            $error = $model->getError() ?: '更新失败';
            return $this->renderError($error);
        }
        return $this->renderSuccess('更新成功');
    }
}
