<?php
namespace app\store\controller\sharewifi;

use app\store\controller\sharewifi\Controller;
use app\store\model\sharewifi\User as UserModel;
use app\store\model\sharewifi\Shop as ShopModel;
use app\store\model\sharewifi\Advert as AdvertModel;
use think\facade\View;


/**
 * 后台首页
 */
class Index extends Controller
{
	
    public function index()
    {
		$count['user'] = UserModel::getCount();	//用户统计
		$count['shop'] = ShopModel::getCount();	//商家统计
		$count['advert'] = AdvertModel::getCount();	//广告统计
		return View::fetch('index', compact('count'));
    }
}
