<?php
namespace app\store\controller\sharewifi;

use app\store\controller\sharewifi\Controller;
use app\store\model\sharewifi\Setting as SettingModel;
use think\facade\View;

/**
 * 系统设置
 */
class Setting extends Controller
{	
    /**
     * 体验用户设置
     */
    public function test()
    {
        return $this->updateEvent('test');
    }

    /**
     * 更新设置事件
     */
    private function updateEvent(string $key)
    {
        if (!$this->request->isAjax()) {
			$model = SettingModel::getItem($key);
            return View::fetch($key, compact('model'));
        }
        $model = new SettingModel;
        if($model->edit($key, $this->postData('data'))) {
            return $this->renderSuccess('更新成功');
        }
        $error = $model->getError() ?: '添加失败';
        return $this->renderError($error);
    }
}
