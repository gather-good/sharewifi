<?php
namespace app\store\controller\sharewifi\shop;

use app\store\controller\sharewifi\Controller;
use app\store\model\sharewifi\Category as CategoryModel;
use think\facade\View;

/**
 * 门店分类
 */
class Category extends Controller
{
    /**
     * 分类列表
     */
    public function index()
    {
        $model = new CategoryModel;
        $list = $model->getList();
        return View::fetch('index', compact('list'));
    }

    /**
     * 删除分类
     */
    public function delete($id)
    {
        $model = CategoryModel::get($id);
        if (!$model->remove()) {
            $error = $model->getError() ?: '删除失败';
            return $this->renderError($error);
        }
        return $this->renderSuccess('删除成功');
    }

    /**
     * 添加分类
     */
    public function add()
    {
        if ($this->request->isPost()) {
            $model = new CategoryModel;
            if ($model->add($this->postData('data'))) {
                return $this->renderSuccess('添加成功', url('sharewifi.shop.category/index'));
            }
            $error = $model->getError() ?: '添加失败';
            return $this->renderError($error);
        }
        return redirect(url('sharewifi.shop.category/index'));
    }

    /**
     * 编辑
     */
    public function edit($id)
    {
        $model = CategoryModel::get($id);
        if ($this->request->isGet()) {
            if($model){
               return $this->renderSuccess('', '', compact('model')); 
            }
            return $this->renderError('获取失败');
        }
        // 更新记录
        if ($model->edit($this->postData('data'))) {
            return $this->renderSuccess('操作成功', url('sharewifi.shop.category/index'));
        }
        $error = $model->getError() ?: '操作失败';
        return $this->renderError($error);
    }

}
