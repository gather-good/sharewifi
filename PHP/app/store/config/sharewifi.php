<?php
return [
    'index' => [
        'name' => '首页',
        'icon' => 'iconshouye',
        'index' => 'sharewifi.index/index',
    ],
    'shop' => [
        'name' => '商家管理',
        'icon' => 'icondianpu',
        'index' => 'sharewifi.shop/cat_no',
		'submenu' => [
		    [
                'name' => '商家列表',
                'active' => false,
                'submenu' => [
					[
						'name' => '待分类商家',
						'index' => 'sharewifi.shop/cat_no',
						'uris' => [
							'sharewifi.shop/cat_no'
						],
					],
					[
						'name' => '已分类商家',
						'index' => 'sharewifi.shop/cat_ok',
						'uris' => [
							'sharewifi.shop/cat_ok'
						],
					],
				],
            ],
		    [
                'name' => '分类管理',
                'index' => 'sharewifi.shop.category/index',
                'uris' => [
                    'sharewifi.shop.category/index',
                    'sharewifi.shop.category/add',
                    'sharewifi.shop.category/edit',
					'sharewifi.shop.category/delete'
                ],
            ],
		],
    ],
    'advert' => [
        'name' => '广告管理',
        'icon' => 'iconyingyongzhongxin',
        'index' => 'sharewifi.advert/advert',
        'submenu' => [
            [
                'name' => '广告设置',
                'index' => 'sharewifi.advert/advert',
            ],
            [
                'name' => '广告统计',
                'index' => 'sharewifi.advert/census',
            ],
        ]
    ],
    'applet' => [
        'name' => '小程序',
        'icon' => 'iconweixinxiaochengxu',
        'color' => '#36b313',
        'index' => 'sharewifi.applet/share',
        'submenu' => [
            [
                'name' => '分享设置',
                'index' => 'sharewifi.applet/share',
            ],
            [
                'name' => '公告管理',
                'index' => 'sharewifi.applet/notice',
            ],
            [
                'name' => '轮播图片',
                'index' => 'sharewifi.applet/swiper',
            ],
            [
                'name' => '帮助中心',
                'index' => 'sharewifi.applet.help/index',
                'uris' => [
                    'sharewifi.applet.help/index',
                    'sharewifi.applet.help/add',
                    'sharewifi.applet.help/edit',
					'sharewifi.applet.help/delete'
                ],
            ],
        ]
    ],
    'user' => [
        'name' => '用户管理',
        'icon' => 'iconyonghuguanli',
        'index' => 'sharewifi.user/index',
        'submenu' => [
            [
                'name' => '用户列表',
                'index' => 'sharewifi.user/index',
            ],
            [
                'name' => '团长申请',
                'index' => 'sharewifi.user.apply/index',
                'uris' => [
                    'sharewifi.user.apply/index',
                    'sharewifi.user.apply/edit',
					'sharewifi.user.apply/delete'
                ],
            ],
            [
                'name' => '提现申请',
                'index' => 'sharewifi.user.wallet/transfer',
                'uris' => [
                    'sharewifi.user.wallet/transfer',
                    'sharewifi.user.wallet/edit',
                ],
            ],
            [
                'name' => '分红记录',
                'index' => 'sharewifi.user.wallet/share',
                'uris' => [
                    'sharewifi.user.wallet/share',
                ],
            ],
            [
                'name' => '连接记录',
                'index' => 'sharewifi.user.connect/index',
            ],
        ]
    ],
    'setting' => [
        'name' => '设置',
        'icon' => 'iconshezhi',
        'index' => 'sharewifi.setting/test',
        'submenu' => [
            [
                'name' => '体验用户',
                'index' => 'sharewifi.setting/test',
            ],
            [
                'name' => '清理缓存',
                'index' => 'sharewifi.setting.cache/clear',
            ],
        ],
    ],
];
