<?php
namespace app\store\model\sharewifi;

use app\common\model\sharewifi\Wallet as WalletModel;

/**
 * 钱包请记录模型
 */
class Wallet extends WalletModel
{
    /**
     * 获取列表
     */
    public function getList($mode = 0, $status = 0)
    {
        $filter = [];
        $mode > 0 && $filter['mode'] = $mode;
        $status > 0 && $filter['status'] = $status;
        return $this->with(['user'])
            ->where($filter)
            ->order('apply_id','desc')
            ->paginate(['list_rows'=>15,'query' => request()->param()]);
    }
    
    /**
     * 编辑
     */
    public function edit($data)
    {
        //申请驳回
        if($data['status'] == 20 and empty($data['reason'])){
            $this->error = '请输入驳回原因';
            return false;
        }
        //已打款
        if($data['status'] == 40){
            //User::where('user_id', $this->user_id)->update(['status' => 20]);
        }
        return $this->save($data) !== false;
    }
    
    /**
     * 删除
     */
    public function remove()
    {
        return $this->delete();
    }
}
