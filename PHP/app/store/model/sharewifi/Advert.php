<?php
namespace app\store\model\sharewifi;

use app\common\model\sharewifi\Advert as AdvertModel;

/**
 * 广告记录模型
 */
class Advert extends AdvertModel
{
    /**
     * 统计数量
     */
    public static function getCount()
    {
        $count = [
            'all' => [
                'show' => self::where('show',1)->count(), 
                'click' => self::where('click',1)->count(),
                'index_index_video' => [
                    'show' => self::where('key','index_index_video')->where('show',1)->count(), 
                    'click' => self::where('key','index_index_video')->where('click',1)->count(),
                ],
                'index_index_popup' => [
                    'show' => self::where('key','index_index_popup')->where('show',1)->count(), 
                    'click' => self::where('key','index_index_popup')->where('click',1)->count(),
                ],
                'shop_index_popup' => [
                    'show' => self::where('key','shop_index_popup')->where('show',1)->count(), 
                    'click' => self::where('key','shop_index_popup')->where('click',1)->count(),
                ],
                'shop_list_video' => [
                    'show' => self::where('key','shop_list_video')->where('show',1)->count(), 
                    'click' => self::where('key','shop_list_video')->where('click',1)->count(),
                ],
                'shop_wifi_video' => [
                    'show' => self::where('key','shop_wifi_video')->where('show',1)->count(), 
                    'click' => self::where('key','shop_wifi_video')->where('click',1)->count(),
                ],
                'shop_wifi_excitation' => [
                    'show' => self::where('key','shop_wifi_excitation')->where('show',1)->count(), 
                    'click' => self::where('key','shop_wifi_excitation')->where('click',1)->count(),
                ],
                'user_index_popup' => [
                    'show' => self::where('key','user_index_popup')->where('show',1)->count(), 
                    'click' => self::where('key','user_index_popup')->where('click',1)->count(),
                ],
                'login_index_video' => [
                    'show' => self::where('key','login_index_video')->where('show',1)->count(), 
                    'click' => self::where('key','login_index_video')->where('click',1)->count(),
                ],
            ],
        ];
     
        //今天统计
        $star = strtotime(date('Y-m-d 00:00:00',time()));
        $count['today'] = [
            'show' => self::where('create_time','>',$star)->where('show',1)->count(), 
            'click' => self::where('create_time','>',$star)->where('click',1)->count(),
        ];
        //昨天统计
        $star = strtotime("-1 day");
        $end = strtotime(date('Y-m-d 00:00:00',time()));
        $count['today2'] = [
            'show' => self::where('create_time','>',$star)->where('create_time','<',$end)->where('show',1)->count(), 
            'click' => self::where('create_time','>',$star)->where('create_time','<',$end)->where('click',1)->count(),
        ];
        //前天统计
        $star = strtotime("-2 day");
        $end = strtotime("-1 day");
        $count['today3'] = [
            'show' => self::where('create_time','>',$star)->where('create_time','<',$end)->where('show',1)->count(), 
            'click' => self::where('create_time','>',$star)->where('create_time','<',$end)->where('click',1)->count(),
        ];
        //-4天统计
        $star = strtotime("-3 day");
        $end = strtotime("-2 day");
        $count['today4'] = [
            'show' => self::where('create_time','>',$star)->where('create_time','<',$end)->where('show',1)->count(), 
            'click' => self::where('create_time','>',$star)->where('create_time','<',$end)->where('click',1)->count(),
        ];
        //-5天统计
        $star = strtotime("-4 day");
        $end = strtotime("-3 day");
        $count['today5'] = [
            'show' => self::where('create_time','>',$star)->where('create_time','<',$end)->where('show',1)->count(), 
            'click' => self::where('create_time','>',$star)->where('create_time','<',$end)->where('click',1)->count(),
        ];
        //-6天统计
        $star = strtotime("-5 day");
        $end = strtotime("-4 day");
        $count['today6'] = [
            'show' => self::where('create_time','>',$star)->where('create_time','<',$end)->where('show',1)->count(), 
            'click' => self::where('create_time','>',$star)->where('create_time','<',$end)->where('click',1)->count(),
        ];
        //-7天统计
        $star = strtotime("-6 day");
        $end = strtotime("-5 day");
        $count['today7'] = [
            'show' => self::where('create_time','>',$star)->where('create_time','<',$end)->where('show',1)->count(), 
            'click' => self::where('create_time','>',$star)->where('create_time','<',$end)->where('click',1)->count(),
        ];
        //本月统计 
        $end = mktime(0,0,0,date('m'),1,date('y'));
        $count['month'] = [
            'show' => self::where('create_time','>',$end)->where('show',1)->count(), 
            'click' => self::where('create_time','>',$end)->where('click',1)->count(),
        ];
        //上月统计  
        $star = mktime(0,0,0,date('m')-1,1,date('y'));
        $count['month2'] = [
            'show' => self::where('create_time','>',$star)->where('create_time','<',$end)->where('show',1)->count(), 
            'click' => self::where('create_time','>',$star)->where('create_time','<',$end)->where('click',1)->count(),
        ];
        return $count;
    }
}
