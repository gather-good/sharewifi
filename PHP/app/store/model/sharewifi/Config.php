<?php
namespace app\store\model\sharewifi;

use app\common\model\Config as ConfigModel;
use think\facade\Session;

/**
 * 站点配置模型
 */
class Config extends ConfigModel
{
    /**
     * 店长登录登录
     */
    public function login($data)
    {
        if(!captcha_check($data['captcha'])){
            $this->error = '验证码错误';
            return false;
        }
        $user = Setting::getItem('test');
        //验证登录的是否为测试账号
        if($user['is_open'] == 1 and $user['user_name'] == $data['user_name']){
            if($user['password'] != $data['password']){
                $this->error = '体验用户密码错误';
                return false; 
            }
        }else{
            if(!$user = $this->where('user_name' , $data['user_name'])->find()){
                $this->error = '账号不存在';
                return false;
            }
            if($user['password'] != hema_hash($data['password'])){
                $this->error = '密码错误';
                return false;
            }
            $user['avatar'] = base_url() . 'assets/img/avatar.png';
        }
        $applet = get_addons_config('sharewifi');
        Session::set('hema_store_sharewifi',[
        	'user' => [
                'user_name' => $user['user_name'],
                'avatar' => $user['avatar'],
            ],
        	'applet' => $applet,
        	'is_login' => true,
        ]);
        return true;
    }
}
