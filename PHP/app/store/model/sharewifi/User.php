<?php
namespace app\store\model\sharewifi;

use app\common\model\sharewifi\User as UserModel;

/**
 * 用户模型
 */
class User extends UserModel
{
    /**
     * 统计数量
     */
    public static function getCount()
    {
        $count['all'] = self::count();
        //新入用户
        //今天统计
        $star = strtotime(date('Y-m-d 00:00:00',time()));
        $count['today'] = self::where('create_time','>',$star)->count();
        //昨天统计
        $star = strtotime("-1 day");
        $end = strtotime(date('Y-m-d 00:00:00',time()));
        $count['today2'] = self::where('create_time','>',$star)->where('create_time','<',$end)->count();
        //前天统计
        $star = strtotime("-2 day");
        $end = strtotime("-1 day");
        $count['today3'] = self::where('create_time','>',$star)->where('create_time','<',$end)->count();
        //-4天统计
        $star = strtotime("-3 day");
        $end = strtotime("-2 day");
        $count['today4'] = self::where('create_time','>',$star)->where('create_time','<',$end)->count();
        //-5天统计
        $star = strtotime("-4 day");
        $end = strtotime("-3 day");
        $count['today5'] = self::where('create_time','>',$star)->where('create_time','<',$end)->count();
        //-6天统计
        $star = strtotime("-5 day");
        $end = strtotime("-4 day");
        $count['today6'] = self::where('create_time','>',$star)->where('create_time','<',$end)->count();
        //-7天统计
        $star = strtotime("-6 day");
        $end = strtotime("-5 day");
        $count['today7'] = self::where('create_time','>',$star)->where('create_time','<',$end)->count();
        //本月统计 
        $end = mktime(0,0,0,date('m'),1,date('y'));
        $count['month'] = self::where('create_time','>',$end)->count();
        //上月统计  
        $star = mktime(0,0,0,date('m')-1,1,date('y'));
        $count['month2'] = self::where('create_time','>',$star)->where('create_time','<',$end)->count();
        return $count;
    }
}
