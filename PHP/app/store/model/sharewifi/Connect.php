<?php
namespace app\store\model\sharewifi;

use app\common\model\sharewifi\Connect as ConnectModel;

/**
 * WiFi连接记录模型
 */
class Connect extends ConnectModel
{
    /**
     * 获取列表
     */
    public function getList()
    {
        return $this->with(['shop','user'])
            ->order('connect_id','desc')
            ->paginate(['list_rows'=>15,'query' => request()->param()]);
    }
}
