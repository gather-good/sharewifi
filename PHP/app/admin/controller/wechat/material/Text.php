<?php
namespace app\admin\controller\wechat\material;

use app\admin\controller\Controller;
use app\admin\model\Material as MaterialModel;
use app\admin\model\MaterialText as MaterialTextModel;
use app\admin\model\Wechat as WechatModel;
use think\facade\View;
/**
 * 图文素材
 */
class Text extends Controller
{
    /**
     * 列表
     */
    public function index()
    {
        $model = new MaterialModel;
        $list = $model->getList(40);
        $wechat = WechatModel::detail();
        return View::fetch('index', compact('list','wechat'));
    }
	
	/**
     * 添加
     */
    public function add()
    {
        $model = new MaterialModel;
        if (!$this->request->isAjax()) {
			$material = $model->getDefault();
            return View::fetch('add', compact('material'));
        }
        if(!$wechat = WechatModel::detail()){
            return $this->renderError('还未绑定公众号');
        }
        if($wechat['status']['value'] == 0){
            return $this->renderError('还未绑定公众号');
        }
        // 新增记录
		if($model->addText($this->postData('data'))){
			return $this->renderSuccess('添加成功', url('wechat.material.text/index'));
		}
        $error = $model->getError() ?: '添加失败';
        return $this->renderError($error);
    }
	
	/**
     * 编辑
     */
    public function edit($id, string $text_no)
    {
        $model = MaterialModel::get($id);
		$text = new MaterialTextModel;
		$material = $text->getList($text_no,$model['name']);
        if (!$this->request->isAjax()) {
            return View::fetch('edit', compact('material'));
        }
        if(!$wechat = WechatModel::detail()){
            return $this->renderError('还未绑定公众号');
        }
        if($wechat['status']['value'] == 0){
            return $this->renderError('还未绑定公众号');
        }
        // 更新记录
        if ($model->editText($this->postData('data'),$text)) {
            return $this->renderSuccess('更新成功', url('wechat.material.text/index'));
        }
        $error = $model->getError() ?: '更新失败';
        return $this->renderError($error);
    }

    /**
     * 删除
     */
    public function delete($id)
    {
        $model = MaterialModel::get($id);
        if (!$model->remove()) {
            $error = $model->getError() ?: '删除失败';
            return $this->renderError($error);
        }
        return $this->renderSuccess('删除成功');
    }
}
