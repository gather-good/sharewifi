<?php
namespace app\admin\controller;

use app\admin\model\Setting as SettingModel;
use think\facade\View;

/**
 * 微信支付配置
 */
class Pay extends Controller
{
    /**
     * 支付设置
     */
    public function webpay()
    {
        return $this->updateEvent('webpay');
    }

    /**
     * 微信支付服务商设置
     */
    public function wxpayisp()
    {
        return $this->updateEvent('wxpayisp');
    }

    /**
     * 更新设置事件
     */
    private function updateEvent($key)
    {
        if (!$this->request->isAjax()) {
            $model = SettingModel::getItem($key,0);
            return View::fetch($key, compact('model'));
        }
        $model = new SettingModel;
        if($model->edit($key, $this->postData('data'))) {
            return $this->renderSuccess('设置成功');
        }
        return $this->renderError('设置失败');
    }

}
