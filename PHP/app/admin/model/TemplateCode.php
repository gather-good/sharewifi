<?php

namespace app\admin\model;

use app\common\model\TemplateCode as TemplateCodeModel;

/**
 * 模板模型
 */
class TemplateCode extends TemplateCodeModel
{
    /**
     * 添加新记录
     */
    public function add(array $data)
    {
        return $this->save($data);
    }

    /**
     * 编辑记录
     */
    public function edit(array $data)
    {
        return $this->save($data) !== false;
    }

    /**
     * 删除
     */
    public function remove()
    {
        return $this->delete();
    }

}
