<?php
namespace app\admin\model;

use app\common\model\User as UserModel;
use think\facade\Db;

/**
 * 用户模型
 */
class User extends UserModel
{
    /**
     * 代理操作
     */
    public function agentAction()
    {
        if($this->status['value'] == 20){
            return $this->save(['status' => 30]) !== false;//设置为代理
        }
        Db::startTrans();// 开启事务
        try {
            $this->save(['status' => 20]);//取消代理
            Applet::where('agent_id',$this->user_id)->update(['agent_id' => 0]);
            User::where('agent_id',$this->user_id)->update(['agent_id' => 0]);
            DivideAccount::where('agent_id',$this->user_id)->delete();
            Template::where('agent_id',$this->user_id)->delete();
            Apply::where('agent_id',$this->user_id)->delete();
            Record::where('user_id',$this->user_id)->where('user_type',30)->delete();
			Db::commit();
            return true;
        } catch (\Exception $e) {
            Db::rollback();
        }
        return false;
		
	}
}
