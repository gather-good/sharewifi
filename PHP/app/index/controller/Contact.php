<?php
namespace app\index\controller;

use think\facade\View;

/**
 * 页面
 */
class Contact extends Controller
{
    public function index()
    {
		View::assign('key', 'contact');
		View::assign('title', '联系我们');
		View::assign('description', '');
		View::assign('keywords', '');
		return View::fetch();
    }
}
