<?php
namespace app\index\model;

use app\common\model\User as UserModel;
use think\facade\Session;

/**
 * 用户模型
 */
class User extends UserModel
{
    /**
     * 商家用户登录
     */
    public function login($data)
    {
        if(!captcha_check($data['captcha'])){
            $this->error = '验证码错误';
            return false;
        }
        // 验证用户名是否正确
		if(!$user = $this->withoutGlobalScope()->where('status','>',10)->where('user_name',$data['user_name'])->find()){
		    $this->error = '用户不存在';
            return false;
		}
		// 验证密码是否正确
		if($user['password'] != hema_hash($data['password'])){
		    $this->error = '密码错误';
            return false;
		}
		// 保存登录状态
		Session::set('hema_user', [
			'user' => $user,
			'is_login' => true,
		]);
		return true;
    }  

}
