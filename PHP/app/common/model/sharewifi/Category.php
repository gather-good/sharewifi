<?php
namespace app\common\model\sharewifi;

use think\facade\Cache;

/**
 * 分类模型
 */
class Category extends BaseModel
{
    // 定义表名
    protected $name = 'sharewifi_category';

    // 定义主键
    protected $pk = 'category_id';

    protected $append = [];
    
    /**
     * 图标
     */
    public function getLogoAttr($value)
    {
        return ['url' => uploads_url() . '/' . $value, 'path' => $value];
    }
    
    /**
     * 所有分类
     */
    public function getList()
    {
        if (!$list = Cache::get('sharewifi_category')) {
            $list = $this->order(['sort','category_id' => 'desc'])->select();
            Cache::set('sharewifi_category', $list);
        }
        return $list;
    }

    /**
     * 添加新记录
     */
    public function add(array $data)
    {
        $this->deleteCache();
        return $this->save($data);
    }

    /**
     * 编辑记录
     */
    public function edit(array $data)
    {
        $this->deleteCache();
        return $this->save($data) !== false;
    }

    /**
     * 删除分类
     */
    public function remove()
    {
        
        $this->deleteCache();
        $model = new Shop;
        $model->where('category_id',$this->category_id)->update(['category_id' => 0]);
        return $this->delete();
    }

    /**
     * 删除缓存
     */
    private function deleteCache()
    {
        return Cache::delete('sharewifi_category');
    }
}
