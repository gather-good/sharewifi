<?php
namespace app\common\model\sharewifi;

/**
 * 钱包请记录模型
 */
class Wallet extends BaseModel
{
    // 定义表名
    protected $name = 'sharewifi_wallet';
    // 定义主键
    protected $pk = 'wallet_id';
    // 追加字段
    protected $append = [];
    
    /**
     * 关联用户表
     */
    public function user()
    {
        return $this->belongsTo('app\\common\\model\\sharewifi\\User','user_id');
    }
    /**
     * 类型
     */
    public function getModeAttr($value)
    {
        $status = [10 => '分红', 20 => '提现'];
        return ['text' => $status[$value], 'value' => $value];
    }
    /**
     * 状态
     */
    public function getStatusAttr($value)
    {
        $status = [10 => '申请中', 20 => '被驳回', 30 => '打款中', 40 => '已打款'];
        return ['text' => $status[$value], 'value' => $value];
    }

}