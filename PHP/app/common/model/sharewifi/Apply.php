<?php
namespace app\common\model\sharewifi;

/**
 * 申请记录模型
 */
class Apply extends BaseModel
{
    // 定义表名
    protected $name = 'sharewifi_apply';
    // 定义主键
    protected $pk = 'apply_id';
    // 追加字段
    protected $append = [];
    
    /**
     * 关联用户表
     */
    public function user()
    {
        return $this->belongsTo('app\\common\\model\\sharewifi\\User','user_id');
    }

    /**
     * 状态
     */
    public function getStatusAttr($value)
    {
        $status = [10 => '申请中', 20 => '被驳回', 30 => '已通过'];
        return ['text' => $status[$value], 'value' => $value];
    }

}