<?php
namespace app\common\model\sharewifi;

/**
 * 广告统计模型
 */
class Advert extends BaseModel
{
    // 定义表名
    protected $name = 'sharewifi_advert';

    // 定义主键
    protected $pk = 'advert_id';

    protected $append = [];

}
