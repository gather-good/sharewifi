<?php
namespace app\common\model\sharewifi;

/**
 * WiFi连接记录模型
 */
class Connect extends BaseModel
{
    // 定义表名
    protected $name = 'sharewifi_connect';
    // 定义主键
    protected $pk = 'connect_id';
    // 追加字段
    protected $append = [];
    
    /**
     * 关联门店表
     */
    public function shop()
    {
        return $this->belongsTo('app\\common\\model\\sharewifi\\Shop','shop_id');
    }
    
    /**
     * 关联用户表
     */
    public function user()
    {
        return $this->belongsTo('app\\common\\model\\sharewifi\\User','user_id');
    }

    /**
     * 状态
     */
    public function getIsSuccessAttr($value)
    {
        $status = ['失败', '成功'];
        return ['text' => $status[$value], 'value' => $value];
    }

}