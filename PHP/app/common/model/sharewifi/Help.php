<?php
namespace app\common\model\sharewifi;

/**
 * 帮助文档模型
 */
class Help extends BaseModel
{
    // 定义表名
    protected $name = 'sharewifi_help';
    // 定义主键
    protected $pk = 'help_id';
    // 追加字段
    protected $append = [];
    
    /**
     * 获取列表
     */
    public function getList()
    {
        return $this->order(['sort','help_id' => 'desc'])
            ->paginate(['list_rows'=>15,'query' => request()->param()]);
    }

    /**
     * 添加
     */
    public function add($data)
    {
        return $this->save($data);
    }
    
    /**
     * 编辑
     */
    public function edit($data)
    {
        return $this->save($data) !== false;
    }
    
    /**
     * 删除
     */
    public function remove()
    {
        return $this->delete();
    }
}