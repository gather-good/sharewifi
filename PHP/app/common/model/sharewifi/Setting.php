<?php
namespace app\common\model\sharewifi;

use think\facade\Cache;

/**
 * 配置模型
 */
class Setting extends BaseModel
{
    // 定义表名
    protected $name = 'sharewifi_setting';
    protected $createTime = false;

    // 追加字段
    protected $append = [];

    /**
     * 设置项描述
     */
    private $describe = [
        'test' => '体验用户',
        'notice' => '站点公告',
        'swiper' => '轮播图片',
        'share' => '分享设置',
        'advert' => '广告管理'
    ];

    /**
     * 获取器: 转义数组格式
     */
    public function getValuesAttr($value)
    {
        return json_decode($value, true);
    }

    /**
     * 修改器: 转义成json格式
     */
    public function setValuesAttr($value)
    {
        return json_encode($value);
    }

    /**
     * 获取指定项设置
     */
    public static function getItem(string $key)
    {
        $data = self::getAll();
        return isset($data[$key]) ? $data[$key]['values'] : [];
    }
    
    /**
     * 全局缓存: 系统设置
     */
    public static function getAll()
    {
        $self = new static;
        if (!$data = Cache::get('sharewifi_setting')) {
            $data = array_column($self::select()->toArray(), null, 'key');
            Cache::set('sharewifi_setting', $data);
        }
        return array_merge_multiple($self->defaultData(), $data);
    }

    /**
     * 获取设置项信息
     */
    public static function detail(string $key)
    {
        return self::where('key',$key)->find();
    }

    /**
     * 更新系统设置
     */
    public function edit(string $key, array $values)
    {
        $model = self::detail($key) ?: $this;
        // 删除系统设置缓存
        Cache::delete('sharewifi_setting');
        return $model->save([
            'key' => $key,
            'describe' => $this->describe[$key],
            'values' => $values,
        ]) !== false;
    }

    /**
     * 默认配置
     */
    public function defaultData()
    {
        return [
            'test' => [
                'key' => 'test',
                'describe' => '体验用户',
                'values' => [
                    'is_open' => 0,
                    'user_name' => 'test',
                    'password' => 'test',
                    'avatar' => base_url() . 'assets/img/avatar.png',
                ],
            ],
            'notice' => [
                'key' => 'notice',
                'describe' => '站点公告',
                'values' => [
                    'is_open' => 1,
                    'text' => '站点公告',
                    'direction' => 'row',//通告滚动模式，row-横向滚动，column-竖向滚动
                    'url' => '',
                ],
            ],
            'swiper' => [
                'key' => 'swiper',
                'describe' => '轮播图片',
                'values' => [
                    [
					    "type" => 'image',
						"image" => base_url() . 'addons/sharewifi/img/swiper.png',
						"url" => '',
						"poster" => base_url() . 'addons/sharewifi/img/swiper.png'
					],
                ],
            ],
            'share' => [
                'key' => 'share',
                'describe' => '分享设置',
                'values' => [
                    "title" => '共享WiFi蹭网赚钱,躺赚一辈子',
					"image" => base_url() . 'addons/sharewifi/img/share.png',
                ],
            ],
            'advert' => [
                'key' => 'advert',
                'describe' => '广告管理',
                'values' => [
                    "index" => [
                        'index' => [
                            'title' => '首页',
                            'video' => [
                                'title' => '视频',
                                'is_open' => 0,
                                'adunit' => ''
                            ],
                            'popup' => [
                                'title' => '插屏',
                                'is_open' => 0,
                                'adunit' => ''
                            ],
                        ],
                    ],
                    "shop" => [
                        'index' => [
                            'title' => '附近门店',
                            'popup' => [
                                'title' => '插屏',
                                'is_open' => 0,
                                'adunit' => ''
                            ],
                        ],
                        'list' => [
                            'title' => '门店分类',
                            'video' => [
                                'title' => '视频',
                                'is_open' => 0,
                                'adunit' => ''
                            ],
                        ],
                        'wifi' => [
                            'title' => 'WiFi连接',
                            'video' => [
                                'title' => '视频',
                                'is_open' => 0,
                                'adunit' => ''
                            ],
                            'excitation' => [
                                'title' => '激励',
                                'is_open' => 0,
                                'adunit' => ''
                            ],
                        ],
                    ],
                    "user" => [
                        'index' => [
                            'title' => '个人中心',
                            'popup' => [
                                'title' => '插屏',
                                'is_open' => 0,
                                'adunit' => ''
                            ],
                        ],
                        
                    ],
                    "login" => [
                        'index' => [
                            'title' => '用户登录',
                            'video' => [
                                'title' => '视频',
                                'is_open' => 0,
                                'adunit' => ''
                            ],
                        ],
                        
                    ],
                ],
            ],
        ];
    }

}
