<?php
namespace app\common\model\sharewifi;

/**
 * 用户模型
 */
class User extends BaseModel
{
    // 定义表名
    protected $name = 'sharewifi_user';
    // 定义主键
    protected $pk = 'user_id';
    // 追加字段
    protected $append = [];

    /**
     * 性别
     */
    public function getGenderAttr($value)
    {
        $status = [0 => '未知', 1 => '先生', 2 => '女士'];
        return $status[$value];
    }
    
    /**
     * 身份
     */
    public function getStatusAttr($value)
    {
        $status = [10 => '普通用户', 20 => '区域团长'];
        return ['text' => $status[$value], 'value' => $value];
    }
    
    /**
     * 用户头像
     */
    public function getAvatarAttr($value)
    {
        empty($value) && $value = base_url() . 'assets/img/avatar.png';
        return $value;
    }
    
    /**
     * 获取列表
     */
    public function getList($user_id = 0,$search = '')
    {
        $model = $this->order('user_id','desc');
        $filter = [];
        $user_id > 0 && $filter['recommender'] = $user_id;
        
        $search = trim($search);
        if(!empty($search)){
            //是否是数字
            if(is_numeric($search)){   
                //是否是手机号
                if(is_phone($search)){
                    $filter['phone'] = $search;
                }else{
                    $filter['user_id'] = $search;
                }
            }else{
               //不是数字   
                $model->where('nickname','like',"%{$search}%");
            }
        }
        // 执行查询
        return $model->where($filter)->paginate(['list_rows'=>15,'query' => request()->param()]);
    }
    
    /**
     * 编辑
     */
    public function edit($data)
    {
        if(isset($data['gender'])){
            switch ($data['gender']) {
                case '先生':
                    $data['gender'] = 1;
                    break;
                case '女士':
                    $data['gender'] = 2;
                    break;
                default:
                    $data['gender'] = 0;
                    break;
            }
        }
        return $this->save($data) !== false;
    }
}