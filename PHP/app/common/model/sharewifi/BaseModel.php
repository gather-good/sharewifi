<?php
namespace app\common\model\sharewifi;

use think\Model;
use think\facade\Session;
use think\facade\Request;

/**
 * 模型基类
 */
class BaseModel extends Model
{
    // 定义表名
    protected $name;

    // 模型别名
    protected $alias = '';

    // 错误信息
    protected $error = '';

     /**
     * 模型基类初始化
     */
    public static function init()
    {
        parent::init();
        self::limit();
    }

    /**
     * 权限验证
     */
    private static function limit()
    {
        $app_name = app_name();
        if ($app_name == 'store') {
            $session = Session::get('hema_store_sharewifi');
            //如果已经登录
            if(isset($session['user']) AND ((int)$session['is_login'] == 1)){
                //验证是否为体验用户
                $user = Setting::getItem('test');
                if($user['is_open'] == 1 and $user['user_name'] == $session['user']['user_name']){
                    if(Request::isPost() && in_array(Request::action(), ['add', 'edit', 'delete','status','test','swiper','notice'])){
                       die(json_encode(['code' => 0, 'msg' => '体验用户，无权操作！'],JSON_UNESCAPED_UNICODE));
                    }
                }
            }
        }
    }

    /**
     * 查找单条记录
     * @param $data
     * @param array $with
     * @return array|static|null
     */
    public static function get($data, $with = [])
    {
        try {
            $query = (new static)->with($with);
            return is_array($data) ? $query->where($data)->find() : $query->find((int)$data);
        } catch (\Exception $e) {
            return false;
        }
    }

    /**
     * 新增hidden属性
     * @param array $hidden
     * @return $this
     */
    protected function addHidden(array $hidden)
    {
        $this->hidden = array_merge($this->hidden, $hidden);
        return $this;
    }

    /**
     * 返回错误信息
     * @return string
     */
    public function getError()
    {
        return empty($this->error) ? false : $this->error;
    }

}
