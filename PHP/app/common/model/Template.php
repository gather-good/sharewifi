<?php
namespace app\common\model;

use think\facade\Cache;

/**
 * 模板市场模型
 */
class Template extends BaseModel
{
    // 定义表名
    protected $name = 'template';

    // 定义主键
    protected $pk = 'template_id';

    // 追加字段
    protected $append = [];
	
	/**
     * 格式化页面数据（读取的时候对数据转换）
     */
    public function getBuySingleAttr($value)
    {
        return json_decode($value, true);
    }
	

    /**
     * 自动转换data为json格式(修改器，保存de时候操作)
     */
    public function setBuySingleAttr($value)
    {
        return json_encode($value);
    }
	
	/**
     * 格式化页面数据（读取的时候对数据转换）
     */
    public function getRenewSingleAttr($value)
    {
        return json_decode($value, true);
    }
	

    /**
     * 自动转换data为json格式(修改器，保存de时候操作)
     */
    public function setRenewSingleAttr($value)
    {
        return json_encode($value);
    }
	
	/**
     * 格式化页面数据（读取的时候对数据转换）
     */
    public function getBuyManyAttr($value)
    {
        return json_decode($value, true);
    }
	

    /**
     * 自动转换data为json格式(修改器，保存de时候操作)
     */
    public function setBuyManyAttr($value)
    {
        return json_encode($value);
    }
	
	/**
     * 格式化页面数据（读取的时候对数据转换）
     */
    public function getRenewManyAttr($value)
    {
        return json_decode($value, true);
    }
	

    /**
     * 自动转换data为json格式(修改器，保存de时候操作)
     */
    public function setRenewManyAttr($value)
    {
        return json_encode($value);
    }
    
    /**
     * 显示状态
     */
    public function getStatusAttr($value)
    {
        $status = [10 => '上架', 20 => '下架'];
        return ['text' => $status[$value], 'value' => $value];
    }
    
    /**
     * 列表
     */
    public function getListAll($agent_id = 0)
    {
        $filter['agent_id'] = $agent_id;
        return $this->where($filter)
            ->order(['sort','template_id' => 'desc'])
            ->paginate(['list_rows'=>15,'query' => request()->param()]);
    }

    /**
     * 列表
     */
    public function getList($agent_id = 0)
    {
        $filter['agent_id'] = $agent_id;
        $filter['status'] = 10;
        return $this->where($filter)
            ->order(['sort','template_id' => 'desc'])
            ->select();
    }

    /**
     * 获取模板
     */
    public static function getAppType(string $app_type, $agent_id = 0)
    {
        $filter['app_type'] = $app_type;
        $filter['agent_id'] = $agent_id;
        return self::where($filter)->find();
    }

    /**
     * 添加
     */
    public function add(array $data)
    {
        //验证是否有重复模板
        $filter['app_type'] = $data['app_type'];
        isset($data['agent_id']) && $filter['agent_id'] = $data['agent_id'];
        if($this->where($filter)->count()){
            $this->error = '该类型模板已存在！';
            return false;
        }
        //如果代理添加，获取代理价格
        if(isset($data['agent_id'])){
            $tpl = self::getAppType($data['app_type']);
            $data['single_price'] = $tpl['single_price'];
            $data['many_price'] = $tpl['many_price'];
        }
        return $this->save($data);
    }

    /**
     * 编辑记录
     */
    public function edit(array $data)
    {
        return $this->save($data) !== false;
    }

    /**
     * 删除分类
     */
    public function remove()
    {
        // 判断是否发布了模板
        if ($tplCount = (new TemplateCode)->withoutGlobalScope()->where(['app_type'=>$this['app_type']])->count()) {
            $this->error = '该模板下存在' . $tplCount . '个已发布的记录，不允许删除';
            return false;
        }
        // 判断是否创建了小程序
        if ($AppletCount = (new Applet)->withoutGlobalScope()->where(['app_type'=>$this['app_type']])->count()) {
            $this->error = '该模板下存在' . $AppletCount . '个小程序，不允许删除';
            return false;
        }
        return $this->delete();
    }
    
    /**
     * 设置上下架
     */
    public function status()
    {
        $this->status['value'] == 10 ? $this->status = 20 :$this->status = 10;
        return $this->save() !== false;
    }
}
