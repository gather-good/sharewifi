<?php
namespace app\common\model;

/**
 * 用户详情模型
 */
class UserDetail extends BaseModel
{
    // 定义表名
    protected $name = 'user_detail';

    // 定义主键
    protected $pk = 'user_detail_id';

    // 追加字段
    protected $append = [];

    /**
     * 营业执照
     */
    public function getLicenseCopyAttr($value)
    {
        $url = uploads_url() . '/' . $value;
        return ['text' => '营业执照', 'value' => $value, 'url' => $url];
    }
    /**
     * 身份证(正面)
     */
    public function getIdCardCopyAttr($value)
    {
        $url = uploads_url() . '/' . $value;
        return ['text' => '身份证(正面)', 'value' => $value, 'url' => $url];
    }

    /**
     * 身份证(反面)
     */
    public function getIdCardNationalAttr($value)
    {
        $url = uploads_url() . '/' . $value;
        return ['text' => '身份证(反面)', 'value' => $value, 'url' => $url];
    }
    /**
     * 特殊资质
     */
    public function getQualificationsAttr($value)
    {
        $url = uploads_url() . '/' . $value;
        return ['text' => '特殊资质', 'value' => $value, 'url' => $url];
    }
    /**
     * 门头照片
     */
    public function getStoreEntrancePicAttr($value)
    {
        $url = uploads_url() . '/' . $value;
        return ['text' => '特殊资质', 'value' => $value, 'url' => $url];
    }
    
    /**
     * 店内照片
     */
    public function getIndoorPicAttr($value)
    {
        $url = uploads_url() . '/' . $value;
        return ['text' => '特殊资质', 'value' => $value, 'url' => $url];
    }

    /**
     * 详情 
     */
    public static function getUserDetail($user_id)
    {
        $self = new static;
        $data = $self->withoutGlobalScope()->where(['user_id' => $user_id])->find();
        if($data){
            return $data;
        }
        return $self->defaultData();
    }

    /**
     * 获取详情信息
     */
    public static function getDetail(array $filter)
    {
        return self::withoutGlobalScope()->where($filter)->order('user_detail_id','desc')->find();
    }

    /**
     * 更新资料
    */ 
    public function action($data,$user_id)
    {
        if(!$user = $this->withoutGlobalScope()->where(['user_id' => $user_id])->find()){
             //新增操作
            $user = new UserDetail;
            $data['user_id'] = $user_id;
            return $user->save($data);
        }
        return $this->withoutGlobalScope()->where('user_id',$user_id)->update($data);
    }

    /**
     * 初始数据
     */
    public function defaultData()
    {
        return [
            'subject_type' => 'SUBJECT_TYPE_INDIVIDUAL',//主体类型
            'license_copy' => [
                'url' => '',//营业执照
                'value' =>''
            ],
            'merchant_name' => '',       //营业执照名称
            'license_number' => '',      //统一社会信用代码
            'id_card_copy' => [
                'url' => '',//身份证正面
                'value' =>''
            ],
            'id_card_national' => [
                'url' => '',//身份证反面
                'value' =>''
            ],
            'id_card_name' => '',      //证件姓名
            'id_card_number' => '', //证件号码
            'id_card_address' => '', //居住地址
            'card_period_begin' => '', //身份证有效期开始时间
            'card_period_end' => '', //身份证有效期结束时间
            'qualifications' => [
                'url' => '',//食品经营许可证
                'value' =>''
            ],
            'store_entrance_pic' => [
                'url' => '',//门头照片
                'value' =>''
            ],
            'indoor_pic' => [
                'url' => '',//店内照片
                'value' =>''
            ],
            'merchant_shortname' => '',//门店简称
            'biz_address_code' => '', //门店所在省市编码
            'biz_store_address' => '', //门店地址
            'legal_persona_wechat' => '',       //微信号
            'mobile_phone' => '',       //联系电话
            'contact_email' => '',      //邮箱
            'account_bank' => '',      //开户银行
            'bank_address_code' => '',      //开户银行省市编码
            'account_number' => '', //银行账号
        ];
    }






















    /**
     * 获取列表
     */
    public function getList()
    {
        $where = [];
        // 执行查询
        return $this->withoutGlobalScope()
            ->where($where)
            ->order('user_detail_id','desc')
            ->paginate(['list_rows'=>15,'query' => request()->param()]);
    }

    /**
     * 获取详情信息
     */
    public static function detail($user_detail_id)
    {
        return self::withoutGlobalScope()->where(['user_detail_id' => $user_detail_id])->find();
    }

}
