<?php
namespace app\api\controller;

use hema\wechat\Pay as WxPay;
use app\api\model\Record as RecordModel;


/**
 * 支付成功异步通知接口
 */
class Notify
{
    /**
     * 站点扫码支付成功异步通知
     */
    public function native()
    {
        $WxPay = new WxPay([]);
        $WxPay->notify(new RecordModel,'','add');
    }
    
    /**
     * 微信小程序充值成功异步通知
     */
    public function wxapp()
    {
        $WxPay = new WxPay([]);
        $WxPay->notify(new RecordModel,'','add');
    }
}
