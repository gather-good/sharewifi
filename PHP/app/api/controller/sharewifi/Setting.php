<?php
namespace app\api\controller\food;

use app\api\controller\food\Controller;
use app\api\model\food\Setting as SettingModel;

/**
 * 商户配置
 */
class Setting extends Controller
{
    /**
     * 订阅消息配置
	 * $order_mode 订单类型
     */
    public function tpl($order_mode,$table_id=0)
    {
        $tpl = [];
		if($values = SettingModel::getItem('wxapptpl',$this->applet_id)){
    		//退款状态提醒
    		if($order_mode == 'refund'){
    			array_push($tpl,$values['refund']);//订单完成
    		}elseif($order_mode == 20){
    		    //如果是外卖
    			array_push($tpl,$values['receive']);//商家接单
    			array_push($tpl,$values['horseman']);//骑手取餐
    			array_push($tpl,$values['delivery']);//订单配送
    		}else{
    		    array_push($tpl,$values['receive']);//商家接单
    			array_push($tpl,$values['finish']);//订单完成
    			$table_id == 0 && array_push($tpl,$values['take']);//取餐提醒
    		}
		}
		return $this->renderSuccess(compact('tpl'));
    }
    
}
