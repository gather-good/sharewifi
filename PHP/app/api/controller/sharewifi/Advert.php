<?php
namespace app\api\controller\sharewifi;

use app\api\controller\sharewifi\Controller;
use app\api\model\sharewifi\Advert as AdvertModel;

/**
 * 广告统计
 */
class Advert extends Controller
{
    /**
     * 添加
     */
    public function add()
    {
        if ($this->request->isPost()) {
            $model = new AdvertModel;
            if($model->add($this->request->post())){
                return $this->renderMsg('上报成功');
            }
            return $this->renderMsg('上报失败');
        }
        return $this->renderMsg('非法请求');
    }
}
