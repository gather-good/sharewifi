<?php
namespace app\api\controller\sharewifi;

use app\api\controller\sharewifi\Controller;
use app\api\model\sharewifi\Setting;

/**
 * 小程序
 */
class Applet extends Controller
{
    /**
     * 小程序基础信息
     */
    public function base()
    {
        $applet = get_addons_config('sharewifi');
        unset($applet['app_id']);
        unset($applet['app_secret']);
        $applet['notice'] = Setting::getItem('notice');
        $applet['swiper'] = Setting::getItem('swiper');
        $applet['share'] = Setting::getItem('share');
        $applet['advert'] = Setting::getItem('advert');
        return $this->renderSuccess(compact('applet'));
    }
}
