<?php
namespace app\api\controller\sharewifi\user;

use app\api\controller\sharewifi\Controller;
use hema\wechat\Wxapp;

/**
 * 用户海报管理
 */
class Poster extends Controller
{
    /**
     * 获取海报
     */
    public function poster()
    {
        if (!$this->request->isPost()) {
            return $this->renderError('非法请求');
        }
        if ($poster = $this->makingPoster($this->request->post())) {
            return $this->renderSuccess(compact('poster'));
        }
        return $this->renderError('海报制作失败');
    }
    
    /**
     * 获取用户二维码
     */
    public function qrcode()
    {
        $wx = new Wxapp(get_addons_config('sharewifi'));
        if(!$path = $wx->getUnlimitedQRCode('sharewifi','user-' . $this->user_id)){
            $this->error = $wx->getError();
            return false;
        }
        $qrcode = [
            'path' => $path,
            'url' => base_url() . $path
        ];
        return $this->renderSuccess(compact('qrcode'));
    }
    
    /**
     * 制作海报
     */
    private function makingPoster($data)
    {
        header('Content-Type: image/png');//输出协议头 
        $image = imagecreate(520,660);//创建画布(宽，高)
        $background_f = imagecolorallocate($image,255,255,255); //白色背景
        imagefill($image,0,0,$background_f);//从坐标0,0开始填充
        $col_0 = imagecolorallocate($image,0,0,0);//文字颜色黑色
        $col_6 = imagecolorallocate($image,102,102,102);//文字颜色灰色
        $b = strlen($data['line1']);//计算有几个汉子
		$x = (520-$b*12)/2;
        imagettftext($image, 25, 0, $x, 70, $col_0,web_path().'/addons/sharewifi/hema.ttf',$data['line1']);
        $b = strlen($data['line2']);//计算有几个汉子
		$x = (520-$b*9.5)/2;
        imagettftext($image, 22, 0, $x, 120, $col_6,web_path().'/addons/sharewifi/hema.ttf',$data['line2']);
        
        $qrcode = imagecreate(350,350);//创建画布
        $qrcode_img=imagecreatefromjpeg(web_path().$data['qrcode']);//载入旧图
        imagecopyresampled($qrcode,$qrcode_img,0,0,0,0,350,350,430,430);//输出图像
        $qrcode_path = web_path().'temp/sharewifi-user'.$this->user_id.'.png';
        imagepng($qrcode,$qrcode_path);//新图，路径
        imagedestroy($qrcode);//销毁对象
        imagedestroy($qrcode_img);//销毁对象
        
        $qrcode = imagecreatefromstring(file_get_contents($qrcode_path));//读取二维码数据流
        //将水印图片复制到目标图片上，最后个参数100是设置透明度，这里实现不透明效果
		imagecopymerge($image, $qrcode, 85, 160, 0, 0, 350, 350, 100);
		imagedestroy($qrcode);//销毁对象
		
		$foot = imagecreate(520,120);//创建画布
		$background_blue = imagecolorallocate($foot,41,121,255); //绿色背景
        imagefill($foot,0,0,$background_blue);//从坐标0,0开始填充
        $col_f = imagecolorallocate($foot,255,255,255);//文字颜色白色
        $b = strlen($data['line3']);//计算有几个汉子
		$x = (520-$b*9)/2;
        imagettftext($foot, 20, 0, $x, 50, $col_f,web_path().'/addons/sharewifi/hema.ttf',$data['line3']);
        $b = strlen($data['line4']);//计算有几个汉子
		$x = (520-$b*9)/2;
        imagettftext($foot, 20, 0, $x, 90, $col_f,web_path().'/addons/sharewifi/hema.ttf',$data['line4']);
        $foot_path = web_path().'temp/sharewifi-user'.$this->user_id.'-test.png';
        imagepng($foot,$foot_path); 
        imagedestroy($foot);//销毁对象
        $foot = imagecreatefromstring(file_get_contents($foot_path));//读取二维码数据流
		imagecopymerge($image, $foot, 0, 540, 0, 0, 520, 120, 100);
		imagedestroy($foot);//销毁对象
		
        $path = 'qrcode/sharewifi/poster-'.$this->user_id.'.png';
        imagepng($image,web_path().$path); 
        imagedestroy($image);//销毁对象

        return $poster = [
                'path' => $path,
                'url' => base_url() . $path
            ];
    }
}
