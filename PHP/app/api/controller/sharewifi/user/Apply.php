<?php
namespace app\api\controller\sharewifi\user;

use app\api\controller\sharewifi\Controller;
use app\api\model\sharewifi\Apply as ApplyModel;

/**
 * 申请记录管理
 */
class Apply extends Controller
{
    /**
     * 详情
     */
    public function  detail()
    {
        $detail = ApplyModel::detail($this->user_id);
        return $this->renderSuccess(compact('detail'));
    }
    
    /**
     * 添加
     */
    public function add()
    {
        $model = new ApplyModel;
        if ($model->add($this->request->post())) {
            return $this->renderMsg('提交成功');
        }
        $error = $model->getError() ?: '提交失败';
		return $this->renderError($error);
    }

    /**
     * 编辑
     */
    public function edit($id)
    {
        $detail = ApplyModel::detail($this->user_id);
        if ($detail->edit($this->request->post())) {
            return $this->renderMsg('提交成功');
        }
        $error = $detail->getError() ?: '提交失败';
		return $this->renderError($error);
    }
}
