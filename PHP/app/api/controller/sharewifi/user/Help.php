<?php
namespace app\api\controller\sharewifi\user;

use app\api\controller\sharewifi\Controller;
use app\api\model\sharewifi\Help as HelpModel;

/**
 * 帮助文档管理
 */
class Help extends Controller
{
    /**
     * 获取列表
     */
    public function lists()
    {
        $model = new HelpModel;
        $list = $model->getList();
        return $this->renderSuccess(compact('list'));
    }
}
