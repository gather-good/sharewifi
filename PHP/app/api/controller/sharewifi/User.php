<?php
namespace app\api\controller\sharewifi;

use app\api\controller\sharewifi\Controller;
use app\api\model\sharewifi\User as UserModel;
use hema\wechat\Map;

/**
 * 用户管理
 */
class User extends Controller
{
    
    /**
     * 用户详情
     */
    public function detail()
    {
        $userInfo = $this->getUserDetail();
        return $this->renderSuccess(compact('userInfo'));
    }
    
    /**
     * 微信小程序登录
     */
    public function wxappLogin()
    {
        $model = new UserModel;
        if ($user = $model->wxappLogin($this->request->post())) {
             return $this->renderSuccess(compact('user'));
        }
        $error = $model->getError() ?: '登录失败';
        return $this->renderError($error);
    }

	/**
     * 获取用户位置
     */
    public function getLocation(string $location)
    {
        $map = new Map;
		if ($location = $map->getLocation($location)){
            return $this->renderSuccess(compact('location'));
        }
		return $this->renderError($map->getError());
    }
    
    /**
     * 获取用户手机号
     */
    public function getPhoneNumber()
    {
        $model = $this->getUserDetail();
        if ($phone = $model->getPhoneNumber($this->request->post())) {
            return $this->renderSuccess(compact('phone'));
        }
        return $this->renderError('手机号获取失败');
    }
}
