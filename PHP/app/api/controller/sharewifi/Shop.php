<?php
namespace app\api\controller\sharewifi;

use app\api\controller\sharewifi\Controller;
use app\api\model\sharewifi\Shop as ShopModel;
use app\api\model\sharewifi\Connect as ConnectModel;

/**
 * 门店控制器
 */
class Shop extends Controller
{
    /**
     * 我的门店列表
     */
    public function lists($category_id=0)
    {
		$model = new ShopModel;
        $list = $model->getList($category_id,$this->user_id,$this->location);
        return $this->renderSuccess(compact('list'));
    }
    /**
     * 我的门店列表
     */
    public function apiList($category_id=0,$sortType='location')
    {
		$model = new ShopModel;
        $list = $model->apiList($category_id,$sortType,$this->location);
        return $this->renderSuccess(compact('list'));
    }
    
    /**
     * 详情
     */
    public function detail($id)
    {
        if (!$this->request->isPost()) {
            (new ShopModel)->where('shop_id', $id)->inc('visit',1)->update();
            if($detail = ShopModel::detail($id,$this->location)){
                return $this->renderSuccess(compact('detail'));
            }
        }
        $error = $model->getError() ?: '获取失败';
		return $this->renderError($error);
    }
	/**
	 * 添加
     */
    public function add()
    {
        $model = new ShopModel;
        if ($model->add($this->request->post(),$this->user_id)) {
            return $this->renderMsg('添加成功');
        }
        $error = $model->getError() ?: '添加失败';
		return $this->renderError($error);
    }

    /**
     * 编辑
     */
    public function edit($id)
    {
        $detail = ShopModel::get($id);
        if (!$this->request->isPost()) {
            return $this->renderSuccess(compact('detail'));
        }
        if ($detail->edit($this->request->post())) {
            return $this->renderMsg('更新成功');
        }
        $error = $detail->getError() ?: '更新失败';
		return $this->renderError($error);
    }
    /**
     * 删除
     */
    public function delete($id)
    {
        $model = ShopModel::get($id);
        if ($model->remove()) {
            return $this->renderMsg('删除成功');
        }
        $error = $model->getError() ?: '删除失败';
		return $this->renderError($error);
    }
    
    /**
	 * 添加连接WiFi记录
     */
    public function connect()
    {
        $model = new ConnectModel;
        if ($model->add($this->request->post())) {
            return $this->renderMsg('连接WiFi记录添加成功');
        }
    }
}
