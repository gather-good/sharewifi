<?php
namespace app\api\controller\sharewifi\shop;

use app\api\controller\sharewifi\Controller;
use app\api\model\sharewifi\Category as CategoryModel;

/**
 * 门店分类管理
 */
class Category extends Controller
{
    /**
     * 获取列表
     */
    public function lists()
    {
        $model = new CategoryModel;
        $list = $model->getList();
        return $this->renderSuccess(compact('list'));
    }
    
    /**
     * 分类详情
     */
    public function detail($id)
    {
        $detail = CategoryModel::get($id);
        return $this->renderSuccess(compact('detail'));
    }

}
