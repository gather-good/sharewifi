<?php
namespace app\api\model;

use app\common\model\Wechat as WechatModel;

class Wechat extends WechatModel
{
    /**
     * 隐藏字段
     */
    protected $hidden = [];
}
