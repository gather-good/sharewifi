<?php
namespace app\api\model;

use app\common\model\Applet as AppletModel;

class Applet extends AppletModel
{
	/**
     * 隐藏字段
     */
    protected $hidden = [
        'access_token',
        'app_id',
        'authorizer_refresh_token',
        'password',
        'user_name',
        'principal_name',
        'agent_id',
        'applet_id',
        'create_time',
        'expires_in',
        'update_time',
        'source'
    ];

}
