<?php
namespace app\api\model\sharewifi;

use app\common\model\sharewifi\Setting as SettingModel;

class Setting extends SettingModel
{
    /**
     * 隐藏字段
     */
    protected $hidden = [
    ];

}
