<?php
namespace app\api\model\sharewifi;

use app\common\model\sharewifi\Shop as ShopModel;

/**
 * 门店模型
 */
class Shop extends ShopModel
{
    /**
     * 隐藏字段
     */
    protected $hidden = [];

}
