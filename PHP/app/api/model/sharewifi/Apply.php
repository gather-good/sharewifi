<?php
namespace app\api\model\sharewifi;

use app\common\model\sharewifi\Apply as ApplyModel;
use think\facade\Db;

/**
 * 申请记录模型
 */
class Apply extends ApplyModel
{
    /**
     * 隐藏字段
     */
    protected $hidden = [];
    
    /**
     * 用户申请详情
     */
    public static function detail($user_id)
    {
        return self::where('user_id',$user_id)->find();
    }
    
    /**
     * 添加
     */
    public function add($data)
    {
        $model = User::get($data['user_id']);
        //开启事务
        Db::startTrans();
        try {
            $model->edit($data);
            $this->save($data);
            Db::commit();
            return true;
        } catch (\Exception $e) {
            Db::rollback();
        }
        return false;
    }
    
    
    /**
     * 编辑
     */
    public function edit($data)
    {
        $model = User::get($data['user_id']);
        //开启事务
        Db::startTrans();
        try {
            $model->edit($data);
            $data['reason'] = '';
            $data['status'] = 10;
            $this->save($data);
            Db::commit();
            return true;
        } catch (\Exception $e) {
            Db::rollback();
        }
        return false;
    }

}