<?php
namespace app\api\model\sharewifi;

use app\common\model\sharewifi\Connect as ConnectModel;

/**
 * WiFi连接记录模型
 */
class Connect extends ConnectModel
{
    /**
     * 隐藏字段
     */
    protected $hidden = [];
    
    /**
     * 添加
     */
    public function add($data)
    {
        return $this->save($data);
    }
}