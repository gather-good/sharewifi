<?php
namespace app\api\model\sharewifi;

use app\common\model\sharewifi\Category as CategoryModel;

class Category extends CategoryModel
{
    /**
     * 隐藏字段
     */
    protected $hidden = [
    ];

}
