<?php
namespace app\api\model\sharewifi;

use app\common\model\sharewifi\Advert as AdvertModel;

/**
 * 广告统计模型
 */
class Advert extends AdvertModel
{
    /**
     * 隐藏字段
     */
    protected $hidden = [];
    
    /**
     * 添加
     */
    public function add($data)
    {
        $ad = Setting::getItem('advert');
        $key = explode('_',$data['key']);
        if($ad[$key[0]][$key[1]][$key[2]]['is_open'] == 0){
            return true;
        }
        $data[$data['type']] = 1;
        return $this->save($data);
    }
}