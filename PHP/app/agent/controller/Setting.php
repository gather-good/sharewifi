<?php
namespace app\agent\controller;

use app\agent\model\User as UserModel;
use think\facade\View;

/**
 * 用户设置
 */
class Setting extends Controller
{

    /**
     * 更新密码
     */
    public function renew()
    {
        $model = UserModel::get($this->user['user']['user_id']);
        if (!$this->request->isAjax()) {
            return View::fetch('renew', compact('model'));            
        }
        if ($model->renew($this->postData('data'))) {
            return $this->renderSuccess('更新成功');
        }
        return $this->renderError($model->getError() ?: '更新失败');
    }

}
