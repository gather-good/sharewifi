<?php
namespace app\agent\controller;

use app\agent\model\User as UserModel;
use app\agent\model\Record as RecordModel;
use app\agent\model\Applet as AppletModel;
use think\facade\View;

/**
 * 财务管理首页
 */
class Finance extends Controller
{
    /**
     * 首页统计
     */
    public function index()
    {
		$count = array();
        $count['applet'] = AppletModel::getCount($this->agent_id); //用户统计
		$count['user'] = UserModel::getCount($this->agent_id);	//用户统计
		$count['record'] = RecordModel::getCount(0,$this->agent_id);		//订单统计
		return View::fetch('index', compact('count'));
    }
}
