<?php
namespace app\agent\controller;

use think\facade\View;

/**
 * 后台首页
 */
class Index extends Controller
{
    
    public function index()
    {
        return View::fetch();
    }
}
