<?php
namespace app\agent\controller\applet;

use app\agent\controller\Controller;
use app\agent\model\Template as TemplateModel;
use think\facade\View;

/**
 * 模板市场控制器
 */
class Template extends Controller
{
    /**
     * 列表
     */
    public function index()
    {
        $model = new TemplateModel;
		$list = $model->getListAll($this->user['user']['user_id']);
        return View::fetch('index', compact('list'));
    }

    /**
     * 删除分类
     */
    public function delete($id)
    {
        $model = TemplateModel::get($id);
        if (!$model->remove()) {
            $error = $model->getError() ?: '删除失败';
            return $this->renderError($error);
        }
        return $this->renderSuccess('删除成功');
    }

    /**
     * 添加
     */
    public function add()
    {
        $model = new TemplateModel;
        if ($this->request->isGet()) {
            $category = $model->getList();
            return $this->renderSuccess('', '', compact('category')); 
        }else{
            $data = $this->postData('data');
            $data['agent_id'] = $this->user['user']['user_id'];
            if ($model->add($data)) {
                return $this->renderSuccess('添加成功', url('applet.template/index'));
            }
            $error = $model->getError() ?: '添加失败';
            return $this->renderError($error);
        }
    }

    /**
     * 编辑
     */
    public function edit($id)
    {
        $model = TemplateModel::get($id);
        if ($this->request->isGet()) {
            return $this->renderSuccess('', '', compact('model')); 
        }else{
            if ($model->edit($this->postData('data'))) {
                return $this->renderSuccess('更新成功', url('applet.template/index'));
            }
            $error = $model->getError() ?: '更新失败';
            return $this->renderError($error);
        }
    }
    
    /**
     * 上架/下架
     */
	public function status($id)
	{
		$model = TemplateModel::get($id);
		if($model->status()){
			return $this->renderSuccess('操作成功');
		}
		$error = $model->getError() ?: '操作失败';
        return $this->renderError($error);
	}

}
