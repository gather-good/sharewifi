<?php
namespace app\agent\controller;

use app\agent\model\User as UserModel;
use think\facade\View;
use think\facade\Session;
use think\captcha\facade\Captcha;

/**
 * 商家后台认证
 */
class Passport extends Controller
{
    /**
     * 生成验证码
     **/
    public function captcha()
    {
        return Captcha::create();
    }
    
    /**
     * 商家用户登录
     */
    public function login()
    {
        if (!$this->request->isAjax()) {
            // 验证登录状态
            if (isset($this->user) AND (int)$this->user['is_login'] === 1) {
                return redirect(url('index/index'));
            }
            View::layout(false);
            return View::fetch();
        }
        $model = new UserModel;
        if ($model->login($this->postData('data'))) {
            return $this->renderSuccess('登录成功', url('index/index'));
        }
        $error = $model->getError() ?: '登录失败';
        return $this->renderError($error);
    }

    /**
     * 退出登录
     */
    public function logout()
    {
        // 清空登录状态
        Session::delete('hema_agent');
        return redirect(url('passport/login'));
    }

}
