<?php
namespace app\agent\controller\finance;

use app\agent\controller\Controller;
use app\agent\model\Record as RecordModel;
use think\facade\View;

/**
 * 流水记录
 */
class Log extends Controller
{
    /**
     * 我的流水
     */
    public function my()
    {
        return $this->lists('我的流水',0,0,0);
    }

    /**
     * 商家流水
     */
    public function store()
    {
        return $this->lists('商家流水',20,0,0);
    }
    
    /**
     * 会员流水
     */
    public function user()
    {
        return $this->lists('会员流水',10,0,0);
    }

    private function lists($title,$user_type, $user_id, $shop_id)
    {
        $model = new RecordModel;
        $list = $model->getList($user_type, $user_id, $shop_id, $this->user['user']['user_id']);
        return View::fetch('index', compact('list','title'));
    }
}
