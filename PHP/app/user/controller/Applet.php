<?php
namespace app\user\controller;

use app\user\model\Applet as AppletModel;
use app\user\model\Template as TemplateModel;
use app\user\model\User as UserModel;
use think\facade\View;
use think\facade\Session;

/**
 * 小程序管理
 */
class Applet extends Controller
{
	/**
     * 获取列表
     */
    public function index()
    {
		$model = new AppletModel;
        $list = $model->getList(0,$this->user_id);
        return View::fetch('index', compact('list'));
    }

    /**
     * 添加
     */
    public function add()
    {
        if (!$this->request->isAjax()) {
            $model = new TemplateModel;
			$list = $model->getList($this->agent_id);
            $user = UserModel::getUser(['user_id' => $this->user_id]);
            $money = $user['money'];
            return View::fetch('add',compact('list','money'));
        }
		$data = $this->postData('data');
		if(!is_phone($data['phone'])){
            return $this->renderError('手机号码格式不对');	
		}
		$data['user_id'] = $this->user_id;
		$data['agent_id'] = $this->agent_id;
		$data['data_type'] = 'add'; //新增
		if(hook($data['app_type'] . '_applet_add', $data)){
		    return $this->renderSuccess('创建成功',url('applet/index'));
		}
		return $this->renderError('创建失败');
    }

    /**
     * 删除
     */
    public function delete($id)
    {
        $applet = AppletModel::get($id);
        if(hook($applet['app_type'] . '_applet_delete', $applet)) {
			return $this->renderSuccess('删除成功');
        }
		return $this->renderError('删除失败');
    }
    
    /**
     * 一键登录 - 小程序管理端
     */
    public function setup($id)
    {
		$data = $this->user;
		$data['applet'] = AppletModel::get($id);
        Session::set('hema_applet',$data);
		return redirect('/applet');
    }
    
    /**
     * 一键登录 - 模板管理端
     */
    public function login($id)
    {
		$applet = AppletModel::get($id);
		$data = $this->user;
		$data['applet'] = $applet;
        Session::set('hema_store_' . $applet['app_type'],$data);
        return redirect('/store/' . $applet['app_type'] . '.index/index');
    }

    /**
     * 升级续费列表
     */
    public function status($id)
    {
		$applet = AppletModel::get($id);
		$template = TemplateModel::getAppType($applet['app_type'],$this->agent_id);
		if (!$this->request->isAjax()) {
            $user = UserModel::getUser(['user_id' => $this->user_id]);
            $money = $user['money'];
			return View::fetch('upgrade',compact('applet','template','money'));
        }
		$data = $this->postData('data');
		// 创建订单
		if($applet->upgrade($data,$template)) {
			return $this->renderSuccess('续费成功', url('applet/index'));
        }
        $error = $applet->getError() ?: '续费失败';
        return $this->renderError($error);
    }
}
