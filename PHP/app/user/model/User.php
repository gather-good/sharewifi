<?php
namespace app\user\model;

use app\common\model\User as UserModel;
use think\facade\Session;

/**
 * 用户模型
 */
class User extends UserModel
{
	/**
     * 代理登陆
     */
    public function agentLogin()
    {
    	if($this->status['value'] != 30){
    		$this->error = '您还不是代理';
            return false;
    	}
        // 保存登录状态
		Session::set('hema_agent', [
			'user' => $this,
			'is_login' => true
		]);
		return true;
    }
}
