<?php

return [
    'index' => [
        'name' => '首页',
        'icon' => 'iconshouye',
        'index' => 'index/index',
    ],
    'applet' => [
        'name' => '小程序',
        'icon' => 'iconweixinxiaochengxu',
        'color' => '#36b313',
        'index' => 'applet/index',
        'submenu' => [
            [
                'name' => '小程序管理',
                'index' => 'applet/index',
                'urls' => [
                    'applet/index',
                    'applet/add',
                    'applet/upgrade',
                    'applet/delete'
                ]
            ],
        ],
    ],
    'money' => [
        'name' => '我的钱包',
        'icon' => 'iconqiandai',
        'index' => 'money/index',
        'submenu' => [
            [
                'name' => '钱包总览',
                'index' => 'money/index',
                'urls' => [
                    'money/index',
                    'money/recharge'
                ]
            ],
            [
                'name' => '交易记录',
                'index' => 'money/log',
                'urls' => [
                    'money/log'
                ]
            ],
        ]
    ],
    'apply' => [
        'name' => '我的申请',
        'icon' => 'iconrenzheng',
        'index' => 'apply/index',
        'submenu' => [
            [
                'name' => '申请列表',
                'index' => 'apply/index',
                'urls' => [
                    'apply/index',
                    'apply/detail'
                ]
            ]
        ]
    ],
    'setting' => [
        'name' => '设置',
        'icon' => 'iconshezhi',
        'index' => 'setting/renew',
        'submenu' => [
            [
                'name' => '修改密码',
                'index' => 'setting/renew',
            ]
        ],
    ],
];
