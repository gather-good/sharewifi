# 共享WiFi微信小程序

#### 介绍

官网地址：[https://www.hemaphp.com/](https://www.hemaphp.com/)

#### 软件架构

基于thinkPHP6开发


#### 系统演示

演示入口：  [https://demo.hemaphp.com/store/sharewifi.passport/login](https://demo.hemaphp.com/store/sharewifi.passport/login)

账号密码：  test

#### 小程序演示

![输入图片说明](%E5%85%B1%E4%BA%ABWiFi.jpg)

#### 商业版购买

![输入图片说明](wechat.png)